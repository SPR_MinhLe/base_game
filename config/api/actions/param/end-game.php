<?php

use Spr\Base\Config\Helper;

$dataParam = array(
	
 	array(
	 	'key' 			=> 'token_key',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'token_secret',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'game_id',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'status',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'level_id',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;