<?php

use Spr\Base\Config\Helper;

$dataParam = array(
	array(
	 	'key' 			=> 'email',
	 	'rules' 		=> 'email',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001',
			'email' => 'message.api.error.000003',
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),array(
	 	'key' 			=> 'google_id',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001',
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;