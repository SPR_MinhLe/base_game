<?php

use Spr\Base\Config\Helper;

$dataParam = array(
	
 	array(
	 	'key' 			=> 'facebook_id',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'first_name',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'last_name',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'last_name' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'gender',
	 	'rules' 		=> '',
	 	'message' 		=> array(
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'nationality',
	 	'rules' 		=> '',
	 	'message' 		=> array(
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'avatar',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'lang',
	 	'rules' 		=> '',
	 	'message' 		=> array(
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'token_key',
	 	'rules' 		=> '',
	 	'message' 		=> array(
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'token_secret',
	 	'rules' 		=> '',
	 	'message' 		=> array(
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;