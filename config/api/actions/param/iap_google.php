<?php

use Spr\Base\Config\Helper;

$dataParam = array(

 	array(
	 	'key' 			=> 'token_key',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'token_secret',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'store',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'product_id',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'transaction_id',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	),array(
	 	'key' 			=> 'purchase_token',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;