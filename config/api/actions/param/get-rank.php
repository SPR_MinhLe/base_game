<?php

use Spr\Base\Config\Helper;

$dataParam = array(
	
 	array(
	 	'key' 			=> 'token_key',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'token_secret',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.api.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'offset',
	 	'rules' 		=> '',
	 	'message' 		=> array(
	 	),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;