<?php
use Spr\Base\Config\Helper;


$dataParam = array(
 	array(
	 	'key' => '_id',
	 	'rules' => 'required',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 	),
	 	'default' => '',
		'htmlentities' => false
 	),
 	array(
	 	'key' => 'amount',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => 0,
		'htmlentities' => false
 	),
 	array(
	 	'key' => 'bonus',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => 10,
		'htmlentities' => false
 	),
 	array(
	 	'key' => 'description',
	 	'rules' => 'required',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 	),
	 	'default' => '',
		'htmlentities' => true
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;
