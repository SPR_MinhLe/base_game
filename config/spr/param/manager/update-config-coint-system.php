<?php
use Spr\Base\Config\Helper;


$dataParam = array(
 	array(
	 	'key' => '_id',
	 	'rules' => 'required',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 	),
	 	'default' => '',
		'htmlentities' => false
 	),
 	array(
	 	'key' => 'coint_bet_win',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => 0,
		'htmlentities' => false
 	),
 	array(
	 	'key' => 'coint_bet_lose',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => 10,
		'htmlentities' => false
 	),
 	array(
	 	'key' => 'coint_reward_singleMode_video',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'coint_reward_singleMode_facebook',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'coint_reward_muntiplayer_video',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'coint_reward_muntiplayer_facebook',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'coint_invited_facebook',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;
