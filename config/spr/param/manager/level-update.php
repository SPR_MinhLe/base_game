<?php
use Spr\Base\Config\Helper;


$dataParam = array(
 	array(
	 	'key' => '_id',
	 	'rules' => 'required',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 	),
	 	'default' => '',
		'htmlentities' => false
 	),
 	// array(
	 // 	'key' => 'level',
	 // 	'rules' => 'required|numeric',
	 // 	'message' => array(
	 // 		'required'	=>	'message.web.error.000001',
	 // 		'numeric'	=>	'message.web.error.000004'
	 // 	),
	 // 	'default' => '',
		// 'htmlentities' => false
 	// ),
 	array(
	 	'key' => 'unlock_requirement',
	 	'rules' => 'numeric',
	 	'message' => array(
	 		// 'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => 0,
		'htmlentities' => false
 	),
 	array(
	 	'key' => 'fall_speed',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => 10,
		'htmlentities' => false
 	),
 	array(
	 	'key' => 'brick_total',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'rarity_J',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'rarity_L',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'rarity_l',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'rarity_Z',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'rarity_S',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'rarity_O',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	),
 	array(
	 	'key' => 'rarity_T',
	 	'rules' => 'required|numeric',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 		'numeric'	=>	'message.web.error.000004'
	 	),
	 	'default' => '',
		'htmlentities' => true
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;
