<?php

use Spr\Base\Config\Helper;

$dataParam = array(

 	array(
	 	'key' 			=> 'username',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'password',
	 	'rules' 		=> 'required',
	 	'message' 		=> array(
			'required' => 'message.error.000001'
	 	),
	 	'default' 		=> '',
		'htmlentities' 	=> false
 	),
 	array(
	 	'key' 			=> 'remember_me',
	 	'rules' 		=> '',
	 	'message' 		=> array(),
	 	'default' 		=> 0,
		'htmlentities' 	=> false
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);
return $dataConfig;