<?php
use Spr\Base\Config\Helper;


$dataParam = array(
 	array(
	 	'key' => '_id',
	 	'rules' => 'required',
	 	'message' => array(
	 		'required'	=>	'message.web.error.000001',
	 	),
	 	'default' => '',
		'htmlentities' => false
 	)
);

$dataConfig = Helper::setDataConfig($dataParam);

return $dataConfig;
