<?php

return [


   'form'	=> [
   		'add_new'	=> [
	   			'title'	=> 'button.form.add_new.title',
	   			'text'	=> 'button.form.add_new.text',
	   			'icon'	=> 'fa fa-plus',
	   			'class'	=> 'btn-create-new'
   		],
   		'view'	=> [
	   			'title'	=> 'button.form.view.title',
	   			'text'	=> 'button.form.view.text',
	   			'icon'	=> 'fa fa-search',
	   			'class'	=> 'btn-view'
   		],
   		'edit'	=> [
	   			'title'	=> 'button.form.edit.title',
	   			'text'	=> 'button.form.edit.text',
	   			'icon'	=> 'fa fa-edit',
	   			'class'	=> 'btn-edit'
   		],
   		'delete'	=> [
	   			'title'	=> 'button.form.delete.title',
	   			'text'	=> 'button.form.delete.text',
	   			'icon'	=> 'fa fa-times',
	   			'class'	=> 'btn-delete'
   		],
   		'submit'	=> [
	   			'title'	=> 'button.form.submit.title',
	   			'text'	=> 'button.form.submit.text',
	   			'icon'	=> 'fa fa-check',
	   			'class'	=> ''
   		],
   		'search'	=> [
	   			'title'	=> 'button.form.search.title',
	   			'text'	=> 'button.form.search.text',
	   			'icon'	=> 'fa fa-search',
	   			'class'	=> 'btn-submit'
   		],
   		'cancel'	=> [
	   			'title'	=> 'button.form.cancel.title',
	   			'text'	=> 'button.form.cancel.text',
	   			'icon'	=> '',
	   			'class'	=> 'btn-cancel'
   		],
   		'reset'	=> [
	   			'title'	=> 'button.form.reset.title',
	   			'text'	=> 'button.form.reset.text',
	   			'icon'	=> '',
	   			'class'	=> 'btn-reset'
   		]
   ]
];
