<?php

return [

    "game_id" 	=> "",
    "user_info"	=> null,
    "activity" 	=> "",
    "db_target" 	=> null,
    "id_target" 	=> null,
    "amount" 	=> 0,
    "price" 	=> 0,
    "status"  	=> 1,
    "level"  	=> 1,
    "created_time" => null
];