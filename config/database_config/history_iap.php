<?php

return [

	'store' 			=> '',
	'product' 			=> '',
	'transaction_id' 	=> '',
	'purchase_token' 	=> '',
	'user' 				=> '',
	'created_time' => null,
	'updated_time' => null,
	'deleted_time' => null
];