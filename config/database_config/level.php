<?php

return [

    'level'         => '',
    'status'        => 0,
    'unlock_requirement' => null,
    'fall_speed'    => 0,
    'brick_total'   => 0,
    'rarity_J'      => 0,
    'rarity_L'      => 0,
    'rarity_l'      => 0,
    'rarity_Z'      => 0,
    'rarity_S'      => 0,
    'rarity_O'      => 0,
    'rarity_T'      => 0,
    'created_time'  => null,
    'updated_time'  => null,
    'deleted_time'  => null
];