<?php

return [

	'first_name' => '',
	'last_name' => '',
	'email' => '',
	'facebook_id' => null,
	'avatar' => '',
	'current_coint' => 0,
	'level_unlocked' => 0,
	'ip_address' => '',
	'nationality' => '',
	'gender'	=> null,
	'token'	=> [
		'key'	=> "",
		'secret' => ""
	],
	'created_time' => null,
	'updated_time' => null,
	'deleted_time' => null
];