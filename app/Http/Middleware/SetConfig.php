<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Models\ConfigGame as ModelConfig;
use App\Http\Models\Activity as ModelActivity;
use App\Http\Models\GamePlay as ModelGamePlay;
use App\Http\Models\Level as ModelLevel;
use App\Http\Models\Item as ModelItem;
use App\Http\Models\Product as ModelProduct;

use Cache;

class SetConfig
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // get config
        Cache::flush();
        $this->setCacheConfig();
        $this->setCacheActivity();
        $this->setCacheGamePlay();
        $this->setCacheItem();
        $this->setCacheLevel();
        $this->setCacheProduct();



        // get activity

        return $next($request);
    }

    public function setCacheLevel () {

        if(!Cache::has('api-level')){

            $ModelLevel = new ModelLevel();
            $data_level = $ModelLevel->getDataLevel();

            if(!empty($data_level['response'])) {

                $data_level_config = [];
                foreach ($data_level['response'] as $key => $value) {

                    $data_level_config[(string)$value['_id']] = $value;
                }
                Cache::forever('api-level', $data_level_config);
            }
        }
    }

    public function setCacheConfig () {

        if(!Cache::has('api-config')){

            $ModelConfig = new ModelConfig();
            $data_config = $ModelConfig->getDataConfig();

            if(!empty($data_config['response'])) {

                Cache::forever('api-config', $data_config['response'][0]);
            }
        }
    }

    public function setCacheActivity () {

        if(!Cache::has('api-activity')){

            $ModelActivity = new ModelActivity();
            $data_activity = $ModelActivity->getDataActivity();

            if(!empty($data_activity['response'])) {

                $data_activity_config = [];
                foreach ($data_activity['response'] as $key => $value) {

                    $data_activity_config[$value['slug']] = [ 'code' => $value['code'], "db_target" => $value['db_target']];
                }
                Cache::forever('api-activity', $data_activity_config);
            }
        }
    }

    public function setCacheGamePlay () {

        if(!Cache::has('api-game-play')){

            $ModelGamePlay = new ModelGamePlay();
            $data_game_play = $ModelGamePlay->getDataGamePlay();

            if(!empty($data_game_play['response'])) {

                $data_game_play_config = [];
                foreach ($data_game_play['response'] as $key => $value) {

                    $data_game_play_config[(string)$value['_id']] = [ 'amount' => $value['amount'], "id" => (string)$value['_id'], "code" => $value['code'], "bonus" => $value['bonus']];
                }
                Cache::forever('api-game-play', $data_game_play_config);
            }
        }
    }

    public function setCacheItem () {
        if(!Cache::has('item')){

            $ModelItem = new ModelItem();
            $data_item = $ModelItem->getDataItem();

            if(!empty($data_item['response'])) {

                $data_item_config = [];
                foreach ($data_item['response'] as $key => $value) {

                    $data_item_config[(string)$value['_id']] = [
                                'price_Base' => $value['price_Base'],
                                "id" => $value['_id'],
                                "name" => $value['name']
                    ];
                }
                Cache::forever('item', $data_item_config);
            }
        }
    }

    public function setCacheProduct () {

        if(!Cache::has('product')){

            $ModelProduct = new ModelProduct();
            $data_product = $ModelProduct->getDataItem();

            if(!empty($data_product['response'])) {

                $data_item_config = [];
                foreach ($data_product['response'] as $key => $value) {

                    $data_item_config[(string)$value['slug']] = [
                                'name'  => $value['name'],
                                "id"    => $value['_id'],
                                "coint" => $value['coint'],
                                "type"  => $value['type'],
                                "slug"  => $value['slug'],
                                "description"  => $value['description'],
                    ];
                }
                Cache::forever('product', $data_item_config);
            }
        }
    }
}
