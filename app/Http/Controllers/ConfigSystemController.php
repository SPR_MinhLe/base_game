<?php

namespace App\Http\Controllers;

use Input;
use App\Http\Models\ConfigGame as ModelConfigGame;
use Spr\Base\Response\Response;
use Cache;

/**
*
*/
class ConfigSystemController extends Controller
{


	function __construct()
	{
		# code...
	}


	public function updateConfigCoint ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']){

			$coint_bet_win                      = (float)$data_output_validate_param['response']['coint_bet_win'];
            $coint_bet_lose                     = (float)$data_output_validate_param['response']['coint_bet_lose'];
            $coint_reward_singleMode_video      = (float)$data_output_validate_param['response']['coint_reward_singleMode_video'];
            $coint_reward_singleMode_facebook   = (float)$data_output_validate_param['response']['coint_reward_singleMode_facebook'];
            $coint_reward_muntiplayer_video     = (float)$data_output_validate_param['response']['coint_reward_muntiplayer_video'];
            $coint_reward_muntiplayer_facebook  = (float)$data_output_validate_param['response']['coint_reward_muntiplayer_facebook'];
            $coint_invited_facebook             = (float)$data_output_validate_param['response']['coint_invited_facebook'];

            $data = [

            	'coint.bet.win' => $coint_bet_win,
            	'coint.bet.lose' => $coint_bet_lose,
                'coint.reward.singleMode.video' => $coint_reward_singleMode_video,
                'coint.reward.singleMode.facebook' => $coint_reward_singleMode_facebook,
                'coint.reward.Muntiplayer.video' => $coint_reward_muntiplayer_video,
                'coint.reward.Muntiplayer.facebook' => $coint_reward_muntiplayer_facebook,
                'coint.invited.facebook' => $coint_invited_facebook,
            ];
            $where = [
            	[
            		'fields' => '_id',
            		'operator' => '=',
            		'value' => (string)$data_output_validate_param['response']['_id']
            	]
            ];
            $ModelConfigGame 	=	new 	ModelConfigGame();

            $data_output_validate_param = $ModelConfigGame->updateData($data, $where);

            $data_config = $ModelConfigGame->getDataConfig();

            if(!empty($data_config['response'])) {

                Cache::forever('api-config', $data_config['response'][0]);
            }
        }else {

            $data_output_validate_param['response'] = array();
        }

		return $data_output_validate_param;
	}

    public function updateServerList ($data_output_validate_param) {

        if($data_output_validate_param['meta']['success']){

            $server_api = $data_output_validate_param['response']['server_api'];
            $server_real_time = $data_output_validate_param['response']['server_real_time'];

            $data = [

                'server_list.api' => $server_api,
                'server_list.real_time' => $server_real_time,
            ];
            $where = [
                [
                    'fields' => '_id',
                    'operator' => '=',
                    'value' => (string)$data_output_validate_param['response']['_id']
                ]
            ];
            $ModelConfigGame  =   new     ModelConfigGame();

            $data_output_validate_param = $ModelConfigGame->updateData($data, $where);
            $data_config = $ModelConfigGame->getDataConfig();

            if(!empty($data_config['response'])) {

                Cache::forever('api-config', $data_config['response'][0]);
            }

        }else {

            $data_output_validate_param['response'] = array();
        }

        return $data_output_validate_param;
    }

}