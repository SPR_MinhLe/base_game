<?php

namespace App\Http\Controllers;

use Input;
// use App\Http\Models\Level as ModelLevel;
use Spr\Base\Response\Response;
use App\Http\Models\User as ModelUser;
use Cache;
/**
*
*/
class FacebookController extends Controller
{


	function __construct()
	{
		# code...
	}

	public function invitedFriends ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']){

			$list_data  = $data_output_validate_param['response'];
			$user_id 	= (string)$data_output_validate_param['data_user']['_id'];
			$config 	= Cache::get('api-config')['coint']['invited']['facebook'];
			$count_data = COUNT($list_data);

			$coint = $count_data * $config;
			$ModelUser = new ModelUser();
			$ModelUser->updateCointEndGame($user_id, $coint);
			$data_output_validate_param['response'] = ['total_coint' => (float)$data_output_validate_param['data_user']['current_coint'] + $coint];
			unset($data_output_validate_param['data_user']);
		}

		return $data_output_validate_param;
	}

}