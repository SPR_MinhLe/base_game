<?php

namespace App\Http\Controllers;

use Input;
use App\Http\Models\GamePlay as ModelGamePlay;
use Spr\Base\Response\Response;


/**
*
*/
class GamePlayController extends Controller
{


	function __construct()
	{
		# code...
	}

	public function getDataManager($data_output_validate_param){

		if($data_output_validate_param['meta']['success']){

			$key_search    	= $data_output_validate_param['response']['key_search'];
        	$sort           = $data_output_validate_param['response']['sort'];
        	$limit          = $data_output_validate_param['response']['limit'];
        	$sort_type      = $data_output_validate_param['response']['sort_type'];

            $ModelGamePlay 	=	new 	ModelGamePlay();

            $data = $ModelGamePlay->getDataManager($key_search, $limit, $sort, $sort_type);

            $data_output_validate_param['response']['data'] = $data;
        }else {

            $data_output_validate_param['response']['data'] = array();
        }
        // dd($data_output_validate_param);
		return $data_output_validate_param;

	}

	public function updateGamePlay ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']){

			$amount = (float)$data_output_validate_param['response']['amount'];
            $bonus = (float)$data_output_validate_param['response']['bonus'];
            $description = $data_output_validate_param['response']['description'];

            $data = [

            	'amount' => $amount,
            	'bonus' => $bonus,
            	'description' => $description,
            ];
            $where = [
            	[
            		'fields' => '_id',
            		'operator' => '=',
            		'value' => (string)$data_output_validate_param['response']['_id']
            	]
            ];
            $ModelGamePlay 	=	new 	ModelGamePlay();

            $data_output_validate_param = $ModelGamePlay->updateData($data, $where);

        }else {

            $data_output_validate_param['response'] = array();
        }

		return $data_output_validate_param;
	}

}