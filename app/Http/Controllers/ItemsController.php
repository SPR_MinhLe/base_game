<?php

namespace App\Http\Controllers;

use Input;
use App\Http\Models\Item as ModelItem;
use Spr\Base\Response\Response;


/**
*
*/
class ItemsController extends Controller
{


	function __construct()
	{
		# code...
	}

	public function getDataManager($data_output_validate_param){

		if($data_output_validate_param['meta']['success']){

			$key_search    	= $data_output_validate_param['response']['key_search'];
        	$sort           = $data_output_validate_param['response']['sort'];
        	$limit          = $data_output_validate_param['response']['limit'];
        	$sort_type      = $data_output_validate_param['response']['sort_type'];

            $ModelItem 	=	new 	ModelItem();

            $data = $ModelItem->getDataManager($key_search, $limit, $sort, $sort_type);

            $data_output_validate_param['response']['data'] = $data;
        }else {

            $data_output_validate_param['response']['data'] = array();
        }
        // dd($data_output_validate_param);
		return $data_output_validate_param;

	}

	public function updateItems ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']){

			$name           = $data_output_validate_param['response']['name'];
            $price_Base     = (float)$data_output_validate_param['response']['price_Base'];
            $description    = $data_output_validate_param['response']['description'];

            $data = [

            	'name'         => $name,
            	'price_Base'   => $price_Base,
            	'description'  => $description,
            ];
            $where = [
            	[
            		'fields' => '_id',
            		'operator' => '=',
            		'value' => (string)$data_output_validate_param['response']['_id']
            	]
            ];
            $ModelItem 	=	new 	ModelItem();

            $data_output_validate_param = $ModelItem->updateData($data, $where);

        }else {

            $data_output_validate_param['response'] = array();
        }

		return $data_output_validate_param;
	}

}