<?php

namespace App\Http\Controllers;

use Input;
use App\Http\Models\LogSingleMode as ModelLogSingleMode;
use App\Http\Models\User as ModelUser;
use App\Http\Controllers\HightCointController;
use App\Http\Controllers\RankController;
use Spr\Base\Response\Response;
use Config;
use Cache;

/**
*
*/
class ActionGameController extends Controller
{


	function __construct()
	{
		# code...
	}

	public function startGame ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']) {

			$ModelLogSingleMode = new ModelLogSingleMode();

			$game_id 		= $data_output_validate_param['response']['game_id'];
            $level_id       = $data_output_validate_param['response']['level_id'];
            $level 			= Cache::get('api-level')[$level_id]['level'];
			$data_user 		= $data_output_validate_param['data_user'];
			$status 		= Cache::get('api-config')['status']['game']['start_game'];
			$activity_code 	= Cache::get('api-activity')['start-game']['code'];
			$db_target 		= Cache::get('api-activity')['start-game']['db_target'];

			$data_output_validate_param = $ModelLogSingleMode->createNewLog($game_id, $data_user, $status, $activity_code, $level, $db_target);
			$data_output_validate_param['response'] = $game_id;
		}

		return $data_output_validate_param;
	}

	// public function endGame ($data_output_validate_param) {

	// 	if($data_output_validate_param['meta']['success']) {

	// 		$ModelLogSingleMode 	= new ModelLogSingleMode();
	// 		$RankController 		= new RankController();
	// 		$HightCointController 	= new HightCointController();
	// 		$ModelUser				= new ModelUser();

	// 		$game_id 		= $data_output_validate_param['response']['game_id'];
 //            $level_id       = $data_output_validate_param['response']['level_id'];
 //            $level 			= (int)Cache::get('api-level')[$level_id]['level'];
	// 		$status 		= $data_output_validate_param['response']['status'];
	// 		$data_user 		= $data_output_validate_param['data_user'];
	// 		$activity_code 	= Cache::get('api-activity')['end-game']['code'];
	// 		$db_target 		= Cache::get('api-activity')['end-game']['db_target'];
	// 		$game_mode  	= null;
	// 		if($game_mode == null || $game_mode == "") $game_mode = Cache::get('api-config')['game_mode']['single'];
	// 		//code xong thi mo cai nay ra
	// 		$data_output_validate_param = $ModelLogSingleMode->createNewLog($game_id, $data_user, $status, $activity_code, $level, $db_target);
	// 		$data_output_validate_param['response'] = [];

	// 		if($data_output_validate_param['meta']['success']) {
	// 			// cong coint
	// 			if($status != Cache::get('api-config')['status']['win_lose']['exit']) {

	// 				$activity_code_get_coint= Cache::get('api-activity')['get-coint']['code'];
	// 				$data_total_coint_get 	= $ModelLogSingleMode->getAllCointOnGameId($game_id, $data_user, $activity_code_get_coint);
	// 				$total_coint 			= (int)$data_total_coint_get['response'];
	// 				$user_id 				= (string)$data_user['_id'];
	// 				if($game_mode == Cache::get('api-config')['game_mode']['single']) {

	// 					$HightCointController->createAndUpdateHightCoint ($user_id, $level, $total_coint);
	// 				}
	// 				$data_rank 				= $RankController->getRankByUserId($data_user);
	// 				$rank_old 				= (int)$data_rank['response']['rank'];
	// 				$level_rank_old 		= (int)$data_rank['response']['level'];
	// 				$coint_rank_old 		= (int)$data_rank['response']['coint'];

	// 				$data_update_rank_current_user = [];

	// 				if(($level_rank_old < $level || $coint_rank_old < $total_coint) && $game_mode == Cache::get('api-config')['game_mode']['single'] ) {

	// 					$data_rank_of_user_max 				= $RankController->getDataMaxRankSmallerThanCurentUser($level, $total_coint);
	// 					$max_rank_smaller_than_current_user = (int)$data_rank_of_user_max['response'];
	// 					$update_reduced_rank_of_other_user 	= $RankController->updateReducedRankOfOtherUser($max_rank_smaller_than_current_user, $rank_old);
	// 					$update_rank_curent_user 			= $RankController->updateDataByUsersId($user_id, $max_rank_smaller_than_current_user, $level, $total_coint);
	// 				}

	// 				if($game_mode != Cache::get('api-config')['game_mode']['single']) {

	// 					$total_coint = $total_coint + ($status == Cache::get('api-config')['status']['win_lose']['win'])?  (int)Cache::get('api-config')['coint']['bet']['win'] : (int)Cache::get('api-config')['coint']['bet']['lose'] ;
	// 				}

	// 				if((int)$data_user['current_coint'] + $total_coint < 0 ) {

	// 					$total_coint = (int)$data_user['current_coint'] * (-1);
	// 				}
	// 				$ModelUser->updateCointEndGame($user_id, $total_coint);


	// 				$next_level = $this->getNextLevel($level_id);
	// 				$level_unlocked = (int)$data_user['level_unlocked'];
	// 				if($status == Cache::get('api-config')['status']['win_lose']['win'] && !empty($next_level) && $level_unlocked < (int)$next_level['level']){

	// 					$data_update = [
	// 						'_id'	=> $user_id,
	// 						'level_unlocked' => $next_level['level']
	// 					];
	// 					$level_unlocked = $next_level['level'];
	// 					$ModelUser->updateData($data_update);

	// 					if($game_mode == Cache::get('api-config')['game_mode']['single']) {

	// 						$hight_coint = $HightCointController->createAndUpdateHightCoint($user_id, $next_level['level'], 0);
	// 					}
	// 				}

	// 				$data_output_validate_param['response']['total_coint'] 	= $total_coint;
	// 				$data_output_validate_param['response']['next_level'] 	= $next_level;
	// 				$data_output_validate_param['response']['level_unlocked'] 	= $level_unlocked;
	// 				$data_output_validate_param['response']['status'] 		= $status;
	// 			}
	// 		}
	// 	}

	// 	unset($data_output_validate_param['data_user']);
	// 	return $data_output_validate_param;
	// }

	public function endGame ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']) {

			$ModelLogSingleMode 	= new ModelLogSingleMode();
			$RankController 		= new RankController();
			$HightCointController 	= new HightCointController();
			$ModelUser				= new ModelUser();

			$game_id 		= $data_output_validate_param['response']['game_id'];
            $level_id       = $data_output_validate_param['response']['level_id'];
            $level 			= (int)Cache::get('api-level')[$level_id]['level'];
			$status 		= $data_output_validate_param['response']['status'];
			$data_user 		= $data_output_validate_param['data_user'];
			$activity_code 	= Cache::get('api-activity')['end-game']['code'];
			$db_target 		= Cache::get('api-activity')['end-game']['db_target'];
			$game_mode  	= null;
			if($game_mode == null || $game_mode == "") $game_mode = Cache::get('api-config')['game_mode']['single'];
			//code xong thi mo cai nay ra
			$data_output_validate_param = $ModelLogSingleMode->createNewLog($game_id, $data_user, $status, $activity_code, $level, $db_target);
			$data_output_validate_param['response'] = [];

			if($data_output_validate_param['meta']['success']) {
				// cong coint
				if($status != Cache::get('api-config')['status']['win_lose']['exit']) {

					$activity_code_get_coint= Cache::get('api-activity')['get-coint']['code'];
					$data_total_coint_get 	= $ModelLogSingleMode->getAllCointOnGameId($game_id, $data_user, $activity_code_get_coint);
					$total_coint 			= (int) $data_total_coint_get['response'];
					$user_id 				= (string) $data_user['_id'];
					$level_unlocked 		= (int) $data_user['level_unlocked'];
					$next_level 			= $this->getNextLevel($level_id);
					$data_rank 				= $RankController->getRankByUserId($data_user);
					$rank_old 				= (int) $data_rank['response']['rank'];
					$level_rank_old 		= (int) $data_rank['response']['level'];
					$coint_rank_old 		= (int) $data_rank['response']['coint'];
					// 1 : update coint + hight coint


					if( $total_coint > 0) {

						$HightCointController->createAndUpdateHightCoint ($user_id, $level, $total_coint);

						if( (int) $data_user['current_coint'] + $total_coint < 0 ) {

							$total_coint = (int) $data_user['current_coint'] * (-1);
						}
						$ModelUser->updateCointEndGame($user_id, $total_coint);
					}

					// 2 : check if level = level unlock
					// check hight coint ton tai
						// true : update
						// false : create
					// update rank

					if($status == Cache::get('api-config')['status']['win_lose']['win'] ){

						if( $level_unlocked <= $level && !empty($next_level) ) {

							$data_update = [
								'_id'	=> $user_id,
								'level_unlocked' => $level
							];
							$level_unlocked = $level;
							$ModelUser->updateData($data_update);

							if($game_mode == Cache::get('api-config')['game_mode']['single']) {

								$hight_coint = $HightCointController->createAndUpdateHightCoint($user_id, (int)$next_level['level'], 0);
							}
						}

						if( ( $level_rank_old < $level || ( $coint_rank_old < $total_coint && $level_rank_old == $level ) ) ) {

							$data_rank_of_user_max 				= $RankController->getDataMaxRankSmallerThanCurentUser($level, $total_coint);
							$max_rank_smaller_than_current_user = (int)$data_rank_of_user_max['response'];
							$update_reduced_rank_of_other_user 	= $RankController->updateReducedRankOfOtherUser($max_rank_smaller_than_current_user, $rank_old);
							$update_rank_curent_user 			= $RankController->updateDataByUsersId($user_id, $max_rank_smaller_than_current_user, $level, $total_coint);
						}
					}

					$data_output_validate_param['response']['total_coint'] 	= $total_coint;
					$data_output_validate_param['response']['next_level'] 	= $next_level;
					$data_output_validate_param['response']['level_unlocked'] 	= $level_unlocked;
					$data_output_validate_param['response']['status'] 		= $status;
				}
			}
		}

		unset($data_output_validate_param['data_user']);
		return $data_output_validate_param;
	}

	public function endGameMultiPlayer ($data_output_validate_param){

		if($data_output_validate_param['meta']['success']) {

			$ModelLogSingleMode 	= new ModelLogSingleMode();
			$ModelUser				= new ModelUser();

			$game_id 		= $data_output_validate_param['response']['game_id'];
            $level_id       = $data_output_validate_param['response']['level_id'];
            $level 			= (int)Cache::get('api-level')[$level_id]['level'];
			$status 		= $data_output_validate_param['response']['status'];
			$data_user 		= $data_output_validate_param['data_user'];
			$activity_code 	= Cache::get('api-activity')['end-game']['code'];
			$db_target 		= Cache::get('api-activity')['end-game']['db_target'];
			// $game_mode  	= $data_output_validate_param['response']['game_mode'];
			// if($game_mode == null || $game_mode == "") $game_mode = Cache::get('api-config')['game_mode']['single'];

			$data_output_validate_param = $ModelLogSingleMode->createNewLog($game_id, $data_user, $status, $activity_code, $level, $db_target);
			$data_output_validate_param['response'] = [];

			if($data_output_validate_param['meta']['success']) {
				// cong coint
				if($status != Cache::get('api-config')['status']['win_lose']['exit']) {

					$activity_code_get_coint= Cache::get('api-activity')['get-coint']['code'];
					$user_id 				= (string)$data_user['_id'];

					$total_coint = ($status == Cache::get('api-config')['status']['win_lose']['win'])?  (int)Cache::get('api-config')['coint']['bet']['win'] : (int)Cache::get('api-config')['coint']['bet']['lose'] ;
					if((int)$data_user['current_coint'] + $total_coint < 0 ) {

						$total_coint = (int)$data_user['current_coint'] * (-1);
					}
					$ModelUser->updateCointEndGame($user_id, $total_coint);
					$data_output_validate_param['response']['total_coint'] 	= $total_coint;
				}
			}
		}

		unset($data_output_validate_param['data_user']);
		return $data_output_validate_param;

	}

	public function getCoint($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']) {

			$ModelLogSingleMode = new ModelLogSingleMode();

			$game_id 		= $data_output_validate_param['response']['game_id'];
			$level_id       = $data_output_validate_param['response']['level_id'];
            $level 			= Cache::get('api-level')[$level_id]['level'];
			$game_play_id	= $data_output_validate_param['response']['game_play_id'];
			$data_user 		= $data_output_validate_param['data_user'];
			unset($data_output_validate_param['data_user']);
			$config_game_play = Cache::get('api-game-play');

			$status 		= Cache::get('api-config')['status']['game']['playing_game'];
			$activity_code 	= Cache::get('api-activity')['get-coint']['code'];
			$db_target 		= Cache::get('api-activity')['get-coint']['db_target'];
			$id_target		= $config_game_play[$game_play_id]["id"];
			$amount			= $config_game_play[$game_play_id]["bonus"];

			$data_output_validate_param = $ModelLogSingleMode->createNewLog($game_id, $data_user, $status, $activity_code, $level, $db_target, $id_target,1, $amount);
			$data_total_coint_get = $ModelLogSingleMode->getAllCointOnGameId($game_id, $data_user, $activity_code);
			$data_output_validate_param['response'] = [ 'total_coint' => (int)$data_total_coint_get['response']];
		}

		return $data_output_validate_param;
	}

	public function useItem($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']) {

			$ModelLogSingleMode = new ModelLogSingleMode();
			$ModelUser = new ModelUser();

			$game_id 		= $data_output_validate_param['response']['game_id'];
			$level_id       = $data_output_validate_param['response']['level_id'];
            $level 			= Cache::get('api-level')[$level_id]['level'];
			$status 		= Cache::get('api-config')['status']['game']['playing_game'];
			$data_user 		= $data_output_validate_param['data_user'];
			$price	 		= (int)$data_output_validate_param['response']['price'];
			$amount 		= $data_output_validate_param['response']['total_use'];
			$price_next_use_item = (int)$data_output_validate_param['response']['price_next_use_item'];

			unset($data_output_validate_param['data_user']);

			$activity_code 	= Cache::get('api-activity')['use-item']['code'];
			$db_target 		= Cache::get('api-activity')['use-item']['db_target'];
			$id_target		= $data_output_validate_param['response']['item_id'];

			$data_output_validate_param = $ModelLogSingleMode->createNewLog($game_id, $data_user, $status, $activity_code, $level, $db_target, $id_target,$amount, $price);

			$current_coint = (int)$data_user['current_coint'] - (int)$price;

			$ModelUser->updateCointUsedItem((string)$data_user['_id'], $price);

			$data_output_validate_param['response'] = ['current_coint' => $current_coint, 'price_next_use_item' => $price_next_use_item];
		}
		return $data_output_validate_param;
	}

	public function getNextLevel ($current_level) {

		$data_level = Cache::get('api-level');
		$next 		= false;
		$next_level = [];

		foreach ($data_level as $key => $value) {

			if($next) {

				$next_level = $value;
				break;
			}

			if((string)$key == (string)$current_level){
				$next_level = $value;
				$next = true;
			}
		}
		return $next_level;
	}

	public function rewardMoreCoin($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']) {

			$ModelLogSingleMode = new ModelLogSingleMode();

			$game_id 		= $data_output_validate_param['response']['game_id'];
			$level_id       = $data_output_validate_param['response']['level_id'];
            $level 			= Cache::get('api-level')[$level_id]['level'];
			$reward_type	= $data_output_validate_param['response']['reward_type'];
			$game_mode		= $data_output_validate_param['response']['game_mode'];
			$status 		= Cache::get('api-config')['status']['game']['reward'];
			$config 		= Cache::get('api-config');
			$activity_list	= Cache::get('api-activity');

			$data_user 		= $data_output_validate_param['data_user'];
			$user_id 		= (string)$data_user['_id'];
			unset($data_output_validate_param['data_user']);
			$type_reward 	= 'video';
			$activity_code 	=  $activity_list['reward-video']['code'];

			if( $config['type']['reward']['facebook'] == $reward_type){

				$activity_code = $activity_list['share-facebook']['code'];
				$type_reward   = 'facebook';
			}


			$amount = ( $game_mode == $config['game_mode']['single'])? $config['coint']['reward']['singleMode'][$type_reward]: $config['coint']['reward']['Muntiplayer'][$type_reward];

			$data_output_validate_param = $ModelLogSingleMode->createNewLog($game_id, $data_user, $status, $activity_code, $level, null, null,1, $amount);
			$ModelUser = new ModelUser();
			$update_coint = $ModelUser->updateCointEndGame($user_id, $amount);

			$data_output_validate_param['response'] = [ 'total_coint' => (int)$data_user['current_coint'] + (int)$amount];
		}

		return $data_output_validate_param;
	}
}