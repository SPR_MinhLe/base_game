<?php

namespace App\Http\Controllers;

use Input;
use App\Http\Models\HightCoint as ModelHightCoint;
use Spr\Base\Response\Response;
use Config;

/**
*
*/
class HightCointController extends Controller
{

	protected $collection = "hight_coint";

	function __construct()
	{
		# code...
	}

	public function createAndUpdateHightCoint ($id_user, $level, $coint) {


		$ModelHightCoint 	= new ModelHightCoint();
		$id_user = (string)$id_user;
		$data_hight_coint 	= $ModelHightCoint->getDataByUserIdAndLevel((string)$id_user, $level);
		$response 			= $data_hight_coint;

		if($data_hight_coint['meta']['success'] && COUNT($data_hight_coint['response']) ) {

			$coint_old = (int)$data_hight_coint['response'][0]['coint'];

			if($coint_old < $coint){

				$data_update = $data_hight_coint['response'][0];
				$data_update['coint'] = $coint;
				$data_update['updated_time'] = strtotime(\Carbon\Carbon::now()->toDateTimeString());
				$ModelHightCoint->updateData($data_update);
			}
		}else {

			$dataConfig = Config::get('database_config.hight_coint');
			$dataConfig['users_id'] = (string)$id_user;
			$dataConfig['level'] = $level;
			$dataConfig['coint'] = $coint;
			$dataConfig['created_time'] = strtotime(\Carbon\Carbon::now()->toDateTimeString());


			$ModelHightCoint->createNewHightCoint($dataConfig);
		}

		$response = $this->getDataHightCointByUserId($id_user);

		return $response;
	}

	public function getDataHightCointByUserId ($users_id) {

		$ModelHightCoint 	= new ModelHightCoint();
		return $ModelHightCoint->getDataByUserId((string)$users_id);
	}

	public function getHightCoint ($data_output_validate_param) {


		if($data_output_validate_param['meta']['success']) {

			$data_user = $data_output_validate_param['data_user'];
            $hight_coint = $this->getDataHightCointByUserId((string)$data_user['_id']);

            $data_output_validate_param['response'] = $hight_coint['response'];
            unset($data_output_validate_param['data_user']);
        }

        return $data_output_validate_param;
	}
}