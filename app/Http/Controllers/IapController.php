<?php

namespace App\Http\Controllers;

use Input;
// use App\Http\Models\Level as ModelLevel;
use Spr\Base\Response\Response;
use App\Http\Models\User as ModelUser;
use App\Http\Models\HistoryIAP as ModelHistoryIAP;
use Cache;
use App\Services\GoogleService;
/**
*
*/
class IapController extends Controller
{


	function __construct()
	{
		# code...
	}

	// Can validate xem don hang nay da duoc ban hay chua
	//
	public function google_iap ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']) {

			$store 			= $data_output_validate_param['response']['store'];
			$product_id		= $data_output_validate_param['response']['product_id'];
			$transaction_id	= $data_output_validate_param['response']['transaction_id'];
			$purchase_token	= $data_output_validate_param['response']['purchase_token'];
			$data_user		= $data_output_validate_param['data_user'];

			if($store == 'GooglePlay') {

				$GoogleService = new GoogleService();
				$subscriptions = $GoogleService->getPurchasesSubscriptions($product_id, $purchase_token);

				if( $subscriptions['meta']['success'] ) {

					$list_product = Cache::get('product');

					if( $subscriptions[ 'response' ]->orderId == $transaction_id && isset( $list_product[ $product_id ] ) ) {

						if( $list_product[ $product_id ][ 'type' ] == 0 || $list_product[ $product_id ][ 'type' ] == 1 ) {

							// by coint
							$ModelHistoryIAP 	= new ModelHistoryIAP();
							$ModelUser 			= new ModelUser();
							$coint 				= ( float ) $list_product[ $product_id ][ 'coint' ];
							$user_id 			= ( string ) $data_user['_id'];

							$ModelHistoryIAP->createNewLog($store, $list_product[ $product_id ], $transaction_id, $purchase_token, $data_user);
							$data_update = $ModelUser->updateCointPurchase($user_id, $coint);

							if( $data_update[ 'meta' ][ 'success' ] ) {

								$total_coint = ( float ) $data_user['current_coint'] 	+ $coint;
								$data_output_validate_param['response'] 				= [];
								$data_output_validate_param['response']['total_coint'] 	= $total_coint;

								unset($data_output_validate_param['data_user']);
							}
						}else {

							// remove ads
						}
					}else {

						$data_output_validate_param['meta']['success'] 	= false;
						$data_output_validate_param['meta']['code'] 	= 500;
						$data_output_validate_param['meta']['msg'] 		= [Lang::get('service/google.error.0001')];
					}
				}else {

					return $subscriptions;
				}
			}
		}

		return $data_output_validate_param;
	}

	public function app_store_iap ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']) {

			$receipt		= $data_output_validate_param['response']['receipt-data'];
			$transaction_id	= $data_output_validate_param['response']['transaction_id'];
			// dd(input::All());
			$password		= 'e9b1d535e322412ca0477185fce997f8';
			$data_user		= $data_output_validate_param['data_user'];
			$url_test		= 'https://sandbox.itunes.apple.com/verifyReceipt';
			$url			= 'https://sandbox.itunes.apple.com/verifyReceipt';

			try {

				$data = array("receipt-data" => $receipt, "password" => $password);
				$data_string = json_encode($data);

				$ch = curl_init($url_test);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				    'Content-Type: application/json',
				    'Content-Length: ' . strlen($data_string))
				);

				$result = json_decode( curl_exec($ch) );
				$list_product = Cache::get('product');
				if( $result->status == 0) {

					$data_iap = $result->receipt->in_app;
					foreach ( $data_iap as $key => $value ) {

						if($value->transaction_id == $transaction_id) {

							$product_id = $value->product_id;
							if( isset( $list_product[ $product_id ] ) ) {
								if( $list_product[ $product_id ][ 'type' ] == 0 || $list_product[ $product_id ][ 'type' ] == 1 ) {

									// by coint
									$ModelHistoryIAP 	= new ModelHistoryIAP();
									$ModelUser 			= new ModelUser();
									$coint 				= ( float ) $list_product[ $product_id ][ 'coint' ];
									$user_id 			= ( string ) $data_user['_id'];

									$ModelHistoryIAP->createNewLog("app-store", $list_product[ $product_id ], $transaction_id, $receipt, $data_user);
									$data_update = $ModelUser->updateCointPurchase($user_id, $coint);

									if( $data_update[ 'meta' ][ 'success' ] ) {

										$total_coint = ( float ) $data_user['current_coint'] 	+ $coint;
										$data_output_validate_param['response'] 				= [];
										$data_output_validate_param['response']['total_coint'] 	= $total_coint;

										unset($data_output_validate_param['data_user']);
									}
								}else {

									// remove ads
								}
							}else {
								$data_output_validate_param['meta']['success'] 	= false;
								$data_output_validate_param['meta']['code'] 	= 500;
								$data_output_validate_param['meta']['msg'] 		= [Lang::get('service/google.error.0001')];
							}
							break;
						}
					}
				}else{

					$data_output_validate_param['meta']['success'] 	= false;
					$data_output_validate_param['meta']['code'] 	= 500;
					$data_output_validate_param['meta']['msg'] 		= [Lang::get('service/google.error.0001')];
				}

			} catch (Exception $e) {

				$data_output_validate_param['meta']['success'] 	= false;
				$data_output_validate_param['meta']['code'] 	= 500;
				$data_output_validate_param['meta']['msg'] 		= [Lang::get('service/google.error.0001')];
			}
		}

		return $data_output_validate_param;
	}

	public function getDataHistory ($data_output_validate_param) {


		if($data_output_validate_param['meta']['success']){

			$key_search    	= $data_output_validate_param['response']['key_search'];
        	$sort           = $data_output_validate_param['response']['sort'];
        	$limit          = $data_output_validate_param['response']['limit'];
        	$sort_type      = $data_output_validate_param['response']['sort_type'];

            $ModelHistoryIAP 	=	new 	ModelHistoryIAP();

            $data = $ModelHistoryIAP->getDataManager($key_search, $limit, $sort, $sort_type);

            $data_output_validate_param['response']['data'] = $data;
        }else {

            $data_output_validate_param['response']['data'] = array();
        }
		return $data_output_validate_param;
	}
}