<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Input;
use Auth;
use Config;
use Redirect;
use Session;
use App\Http\Models\Manager as ModelManager;
use Hash;
use App;
use Lang;

class ManagerController extends Controller
{


    public function login($data_output_validate_param) {

        if($data_output_validate_param['meta']['success']){

            $username = $data_output_validate_param['response']['username'];
            $password = $data_output_validate_param['response']['password'];
            $remember = $data_output_validate_param['response']['remember_me'];

            if (!Auth::guard('manager')->attempt(['username' => $username, 'password' => $password])) {

                if (!Auth::guard('manager')->attempt(['email' => $username, 'password' => $password])) {

                    $data_output_validate_param['meta']['success'] = false;
                    $data_output_validate_param['meta']['code'] = '000005';
                    $data_output_validate_param['meta']['msg'] =  ['login' => Lang::get('message.web.error.000005')];
                    $data_output_validate_param['response'] = [];
                }
            }
        }
        
        return $data_output_validate_param;
    }

    public function changePassword($data_output_validate_param) {

        if ($data_output_validate_param['meta']['success']) {

            $old_password           = $data_output_validate_param['response']['old_password'];
            $new_password           = $data_output_validate_param['response']['new_password'];
            $new_password_retype    = $data_output_validate_param['response']['new_password_retype'];
            $user_id                = (string)Auth::guard('manager')->user()['_id'];
            $ModelManager              = new ModelManager();

            $where = [
                [
                    'fields'    => '_id',
                    'operator'  => '=',
                    'value'     => $user_id
                ]
            ];

            if (isset($old_password) && isset($new_password) && isset($new_password_retype) && $old_password != '' && $new_password != '' && $new_password_retype != '') {

                $user               = $ModelManager->selectData( $where);
                $stored_password    = $user['response'][0][ 'password' ];

                if (Hash::check($old_password, $stored_password)) {

                    if ($new_password !== $new_password_retype) {

                        $data_output_validate_param['meta']['code'] = 500;
                        $data_output_validate_param['meta']['msg']  = [ Lang::get('message.web.error.0014') ];

                        return $data_output_validate_param;

                    } else {

                        $data = [
                            'password'    => Hash::make($new_password)
                        ];
                    }
                } else {

                    $data_output_validate_param['meta']['code'] = 500;
                    $data_output_validate_param['meta']['msg']  = [ Lang::get('message.web.error.0025') ];

                    return $data_output_validate_param;
                }
            } else {

                $data_output_validate_param['meta']['code'] = 500;
                $data_output_validate_param['meta']['msg']  = [ Lang::get('message.web.error.0026') ];

                return $data_output_validate_param;
            }

            $results  = $ModelManager->updateData($data, $where);

            $data_output_validate_param['meta']['msg']      = [ Lang::get('message.web.success.0003') ];
            $data_output_validate_param['response']['data'] = $results;
        }

        return $data_output_validate_param;
    }
}