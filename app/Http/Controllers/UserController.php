<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Input;
use Auth;
use Config;
use Redirect;
use Session;
use Spr\Base\Models\HelperMongo;
use App\Http\Models\User as ModelUser;
use App\Http\Controllers\HightCointController;
use App\Http\Controllers\RankController;
use App\Http\Models\HistoryMultiPayler as ModelHistoryMultiPayler;
use Spr\Base\Controllers\Http\Request as RequestBase;
use DB;
use Hash;
use App;
use Lang;

class UserController extends Controller
{

    protected $table = 'users';


    public function getDataManager($data_output_validate_param){

        if($data_output_validate_param['meta']['success']){

            $key_search     = $data_output_validate_param['response']['key_search'];
            $sort           = $data_output_validate_param['response']['sort'];
            $limit          = $data_output_validate_param['response']['limit'];
            $sort_type      = $data_output_validate_param['response']['sort_type'];

            $ModelUser  =   new     ModelUser();

            $data = $ModelUser->getDataManager($key_search, $limit, $sort, $sort_type);

            $data_output_validate_param['response']['data'] = $data;
        }else {

            $data_output_validate_param['response']['data'] = array();
        }
        // dd($data_output_validate_param);
        return $data_output_validate_param;

    }

    public function getDataByEmail($data_output_validate_param) {

        if($data_output_validate_param[ 'meta' ][ 'success' ]) {

            $ModelUser = new ModelUser();
            $HightCointController = new HightCointController();
            $RankController = new RankController();

            $email = $data_output_validate_param[ 'response' ][ 'email' ];
            $google_id = $data_output_validate_param[ 'response' ][ 'google_id' ];

            if($email != '') {

                $data_user = $ModelUser->getDataByEmail($email);
            }else {

                $data_user = $ModelUser->getDataByGoogleId($google_id);
            }

            $flags = RequestBase::ip_info("Visitor", "Country Code");
            $nationality    = ( $flags == null )? 'vn' : strtolower( $flags );

            if($data_user[ 'meta' ][ 'success' ] && count($data_user[ 'response' ]) > 0){

                $data_output_validate_param[ 'response' ] = $data_user[ 'response' ][0];
                $token_key = bin2hex(random_bytes(16));
                $token_secret = bin2hex(random_bytes(16));

                $data_update   = [
                    '_id'   =>  (string)$data_user[ 'response' ][0][ '_id' ],
                    'token' => [
                        'key'    => $token_key,
                        'secret' => $token_secret,
                    ],
                    'google_id'  => $google_id,
                    'nationality' => $nationality
                ];

                $ModelUser->updateData($data_update);
                $data_output_validate_param[ 'response' ][ 'token' ] = $data_update[ 'token' ];

                $hight_coint = $HightCointController->getDataHightCointByUserId($data_user[ 'response' ][0][ '_id' ]);
                $data_output_validate_param[ 'response' ][ 'hight_coint' ] = $hight_coint[ 'response' ];

            }else {
                $count                              = $ModelUser->getCounter();
                $configData                         = Config::get('database_config.users');
                $configData[ 'email' ]              = $email;
                $configData[ 'google_id' ]          = $google_id;
                $configData[ 'first_name' ]         = 'Guest';
                $configData[ 'created_time' ]       = strtotime(\Carbon\Carbon::now()->toDateTimeString());
                $configData[ 'token' ][ 'key' ]     = bin2hex(random_bytes(16));
                $configData[ 'token' ][ 'secret' ]  = bin2hex(random_bytes(16));
                $configData[ 'last_name' ]          = substr(Config::get('api.system.default_variable.count-number-id-guest'), $count) . $count;
                $configData[ 'nationality' ]        = $nationality;

                $data_out_put_insert        = $ModelUser->registerNewAccount($configData);
                $configData[ '_id' ]        = $data_out_put_insert[ 'response' ];
                $data_output_validate_param[ 'response' ] = $configData;
                $hight_coint = $HightCointController->createAndUpdateHightCoint($configData[ '_id' ], 1, 0);
                $rank        = $RankController->createRank($configData,0,0);
                $data_output_validate_param[ 'response' ][ 'hight_coint' ] = $hight_coint[ 'response' ];
            }

            if( $flags != null ) {
                $data_output_validate_param[ 'response' ][ 'flags' ]       = url('assets/global/img/flags/' . strtolower($nationality) . '.png');
            }else {
                $data_output_validate_param[ 'response' ][ 'flags' ]       = url('assets/global/img/flags/vn.png');
            }
        }

        return $data_output_validate_param;
    }

    public function loginFacebook($data_output_validate_param) {

        $response = $data_output_validate_param;
        if($data_output_validate_param[ 'meta' ][ 'success' ]) {

            $flags =  RequestBase::ip_info("Visitor", "Country Code");

            $ModelUser      = new ModelUser();
            $HightCointController = new HightCointController();
            $facebook_id    = $data_output_validate_param[ 'response' ][ 'facebook_id' ];
            $first_name     = $data_output_validate_param[ 'response' ][ 'first_name' ];
            $last_name      = $data_output_validate_param[ 'response' ][ 'last_name' ];
            $gender         = $data_output_validate_param[ 'response' ][ 'gender' ];
            $nationality    = ( $flags == null )? 'vn' : strtolower( $flags );
            $avatar         = $data_output_validate_param[ 'response' ][ 'avatar' ];
            $lang           = $data_output_validate_param[ 'response' ][ 'lang' ];
            $token_key      = $data_output_validate_param[ 'response' ][ 'token_key' ];
            $token_secret   = $data_output_validate_param[ 'response' ][ 'token_secret' ];


            if($lang != null && $lang != '' && ( $lang == 'en-us' || $lang != 'vn-vi') ){

                App::setLocale($lang);
            }

            $data_user_by_facebook_id = $ModelUser->getDataByFacebookId($facebook_id);
            // Ton tai tai khoan voi id facebook trung khop se lay du lieu
            if( $data_user_by_facebook_id[ 'meta' ][ 'success' ] ){

                if(COUNT( $data_user_by_facebook_id[ 'response' ]) > 0){

                    $token_key                              = bin2hex(random_bytes(16));
                    $token_secret                           = bin2hex(random_bytes(16));
                    $response[ 'response' ]                   = $data_user_by_facebook_id[ 'response' ][0];
                    $response[ 'response' ][ 'first_name' ]     = $first_name;
                    $response[ 'response' ][ 'token' ][ 'key' ]   = $token_key;
                    $response[ 'response' ][ 'token' ][ 'secret' ]= $token_secret;
                    $response[ 'response' ][ 'last_name' ]      = $last_name;
                    $response[ 'response' ][ 'gender' ]         = $gender;
                    $response[ 'response' ][ 'nationality' ]    = $nationality;
                    $response[ 'response' ][ 'avatar' ]         = $avatar;

                    $hight_coint = $HightCointController->getDataHightCointByUserId($response[ 'response' ][ '_id' ]);
                    $response[ 'response' ][ 'hight_coint' ]    = $hight_coint[ 'response' ];
                    $ModelUser->updateData($response[ 'response' ]);
                }else {

                    $data_user_by_token = $ModelUser->getDataByToken($token_key, $token_secret);
                    // Neu khong co tai khoan nao voi token nhan duoc
                    // Hoac du lieu token co duoc da duoc len ket voi tai khoan fb khac
                    // => Tao moi tai khoan voi fb id
                    $RankController = new RankController();
                    if( COUNT( $data_user_by_token[ 'response' ] ) == 0 || $data_user_by_token[ 'response' ][ 'facebook_id' ] != null) {

                        $configData                     = Config::get('database_config.users');
                        $configData[ 'email' ]            = null;
                        $configData[ 'facebook_id' ]      = $facebook_id;
                        $configData[ 'first_name' ]       = $first_name;
                        $configData[ 'last_name' ]        = $last_name;
                        $configData[ 'gender' ]           = $gender;
                        $configData[ 'nationality' ]      = $nationality;
                        $configData[ 'avatar' ]           = $avatar;
                        $configData[ 'created_time' ]     = strtotime(\Carbon\Carbon::now()->toDateTimeString());
                        $configData[ 'token' ][ 'key' ]     = bin2hex(random_bytes(16));
                        $configData[ 'token' ][ 'secret' ]  = bin2hex(random_bytes(16));

                        $data_out_put_insert     = $ModelUser->registerNewAccount($configData);
                        $configData[ '_id' ]       = $data_out_put_insert[ 'response' ];
                        $response[ 'response' ]    = $configData;

                        $hight_coint = $HightCointController->createAndUpdateHightCoint($configData[ '_id' ], 1, 0);
                        $rank        = $RankController->createRank($configData,1,0);
                        $response[ 'response' ][ 'hight_coint' ] = $hight_coint[ 'response' ];
                    }else {

                        // Neu co tai khoan voi token nhan duoc
                        // va fb id chua ton tai thi update
                        // => update va get data
                        $response[ 'response' ]                   = $data_user_by_token[ 'response' ];
                        $token_key                              = bin2hex(random_bytes(16));
                        $token_secret                           = bin2hex(random_bytes(16));
                        $response[ 'response' ][ 'first_name' ]     = $first_name;
                        $response[ 'response' ][ 'token' ][ 'key' ]   = $token_key;
                        $response[ 'response' ][ 'token' ][ 'secret' ]= $token_secret;
                        $response[ 'response' ][ 'facebook_id' ]    = $facebook_id;
                        $response[ 'response' ][ 'last_name' ]      = $last_name;
                        $response[ 'response' ][ 'gender' ]         = $gender;
                        $response[ 'response' ][ 'nationality' ]    = $nationality;
                        $response[ 'response' ][ 'avatar' ]         = $avatar;
                        $hight_coint = $HightCointController->getDataHightCointByUserId($response[ 'response' ][ '_id' ]);
                        $response[ 'response' ][ 'hight_coint' ]    = $hight_coint[ 'response' ];
                        $ModelUser->updateData($response[ 'response' ]);
                    }
                }
            }
        }

        return $response;
    }

    public function linkFacebook($data_output_validate_param) {

        $response = $data_output_validate_param;
        if($data_output_validate_param[ 'meta' ][ 'success' ]) {

            $flags          =  RequestBase::ip_info("Visitor", "Country Code");
            $ModelUser      = new ModelUser();
            $HightCointController = new HightCointController();
            $facebook_id    = $data_output_validate_param[ 'response' ][ 'facebook_id' ];
            $first_name     = $data_output_validate_param[ 'response' ][ 'first_name' ];
            $last_name      = $data_output_validate_param[ 'response' ][ 'last_name' ];
            $gender         = $data_output_validate_param[ 'response' ][ 'gender' ];
            $nationality    = ( $flags == null )? 'vn' : strtolower( $flags );
            $avatar         = $data_output_validate_param[ 'response' ][ 'avatar' ];
            $lang           = $data_output_validate_param[ 'response' ][ 'lang' ];
            $token_key      = $data_output_validate_param[ 'response' ][ 'token_key' ];
            $token_secret   = $data_output_validate_param[ 'response' ][ 'token_secret' ];


            if($lang != null && $lang != '' && ( $lang == 'en-us' || $lang != 'vn-vi') ){

                App::setLocale($lang);
            }

            $response[ 'response' ]                     = $data_output_validate_param[ 'data_user' ];
            $response[ 'response' ][ 'first_name' ]     = $first_name;
            $response[ 'response' ][ 'last_name' ]      = $last_name;
            $response[ 'response' ][ 'gender' ]         = $gender;
            $response[ 'response' ][ 'nationality' ]    = $nationality;
            $response[ 'response' ][ 'avatar' ]         = $avatar;
            $ModelUser->updateData($response[ 'response' ]);

            unset($response[ 'data_user' ]);
            return $response;
        }

        return $response;
    }

    public function getUserInformation($data_output_validate_param) {

        if($data_output_validate_param[ 'meta' ][ 'success' ]) {

            $user_id = (string)$data_output_validate_param[ 'response' ][ 'user_id' ];

            $ModelUser = new ModelUser();
            $HightCointController = new HightCointController();
            $dataUser  = $ModelUser->getDataById($user_id);

            if($dataUser[ 'meta' ][ 'success' ] && COUNT($dataUser[ 'response' ]) > 0 ){

                $data_output_validate_param[ 'response' ] = [];
                $data_output_validate_param[ 'response' ][ 'information_user' ] = $dataUser[ 'response' ][0];

                $hight_coint = $HightCointController->getDataHightCointByUserId($dataUser[ 'response' ][0][ '_id' ]);
                $data_output_validate_param[ 'response' ][ 'hight_coint_user' ] = $hight_coint[ 'response' ];

                $flags = ( isset( $dataUser[ 'response' ][0][ 'flags' ] ) )? $dataUser[ 'response' ][0][ 'flags' ] : 'vn';
                $ModelHistoryMultiPayler = new ModelHistoryMultiPayler();
                $dataHistory             = $ModelHistoryMultiPayler->getDataHistory($user_id);
                $win                     = 0;
                $lose                    = 0;

                foreach ($dataHistory[ 'response' ] as $key => $value) {

                    if($value[ 'winer' ] == $user_id){

                        $win = $win + 1;
                    }else {
                        $lose = $lose + 1;
                    }
                }

                $data_output_validate_param[ 'response' ][ 'information_user' ][ 'total_win' ]  = $win;
                $data_output_validate_param[ 'response' ][ 'information_user' ][ 'total_lose' ] = $lose;

                if( $flags != null ) {
                    $data_output_validate_param[ 'response' ][ 'flags' ]       = url('assets/global/img/flags/' . strtolower($flags) . '.png');
                }else {
                    $data_output_validate_param[ 'response' ][ 'flags' ]       = url('assets/global/img/flags/vn.png');
                }

            }else {

                $data_output_validate_param[ 'meta' ][ 'success' ]  = false;
                $data_output_validate_param[ 'meta' ][ 'msg' ]      = [ 'user' => Lang::get('message.api.error.000004')];
                $data_output_validate_param[ 'meta' ][ 'code' ]     = 000004;
                $data_output_validate_param[ 'response' ]         = [];
            }
        }

        unset($data_output_validate_param[ 'data_user' ]);
        return $data_output_validate_param;
    }

    public function blockUser($data_output_validate_param) {

        if($data_output_validate_param[ 'meta' ][ 'success' ]) {

            $_id = (string)$data_output_validate_param[ 'response' ][ '_id' ];

            $ModelUser = new ModelUser();

            $data_update = [
                '_id'       => $_id,
                'blocked'   => true
            ];

            $ModelUser->updateData($data_update);

            $data_output_validate_param['meta']['msg'] = ['block' => Lang::get('message.web.success.000001') ];
        }

        return $data_output_validate_param;
    }
}