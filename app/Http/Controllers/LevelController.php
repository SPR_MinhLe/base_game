<?php

namespace App\Http\Controllers;

use Input;
use App\Http\Models\Level as ModelLevel;
use Spr\Base\Response\Response;


/**
*
*/
class LevelController extends Controller
{

	protected $collection = "level";

	function __construct()
	{
		# code...
	}

	public function getDataManager($data_output_validate_param){

		if($data_output_validate_param['meta']['success']){

			$key_search    	= $data_output_validate_param['response']['key_search'];
        	$sort           = $data_output_validate_param['response']['sort'];
        	$limit          = $data_output_validate_param['response']['limit'];
        	$sort_type      = $data_output_validate_param['response']['sort_type'];

            $ModelLevel 	=	new 	ModelLevel();

            $data = $ModelLevel->getDataManager($key_search, $limit, $sort, $sort_type);

            $data_output_validate_param['response']['data'] = $data;
        }else {

            $data_output_validate_param['response']['data'] = array();
        }
        // dd($data_output_validate_param);
		return $data_output_validate_param;

	}

	public function updateLevel ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']){

			$rarity_J = (float)$data_output_validate_param['response']['rarity_J'];
            $rarity_L = (float)$data_output_validate_param['response']['rarity_L'];
            $rarity_l = (float)$data_output_validate_param['response']['rarity_l'];
            $rarity_Z = (float)$data_output_validate_param['response']['rarity_Z'];
            $rarity_S = (float)$data_output_validate_param['response']['rarity_S'];
            $rarity_O = (float)$data_output_validate_param['response']['rarity_O'];
            $rarity_T = (float)$data_output_validate_param['response']['rarity_T'];
            // $level = (float)$data_output_validate_param['response']['level'];
            $unlock_requirement = (float)$data_output_validate_param['response']['unlock_requirement'];
            $fall_speed = (float)$data_output_validate_param['response']['fall_speed'];
            $brick_total = (float)$data_output_validate_param['response']['brick_total'];


            $data = [

            	'rarity_J' => $rarity_J,
            	'rarity_L' => $rarity_L,
            	'rarity_l' => $rarity_l,
            	'rarity_Z' => $rarity_Z,
            	'rarity_S' => $rarity_S,
            	'rarity_O' => $rarity_O,
            	'rarity_T' => $rarity_T,
            	// 'level' => $level,
            	'unlock_requirement' => $unlock_requirement,
            	'fall_speed' => $fall_speed,
            	'brick_total' => $brick_total
            ];
            $where = [
            	[
            		'fields' => '_id',
            		'operator' => '=',
            		'value' => (string)$data_output_validate_param['response']['_id']
            	]
            ];
            $ModelLevel 	=	new 	ModelLevel();

            $data_output_validate_param = $ModelLevel->updateData($data, $where);

        }else {

            $data_output_validate_param['response'] = array();
        }

		return $data_output_validate_param;
	}

}