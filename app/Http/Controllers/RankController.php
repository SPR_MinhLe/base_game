<?php

namespace App\Http\Controllers;

use Input;
use App\Http\Models\Rank as ModelRank;
use Spr\Base\Response\Response;
use App\Http\Models\User as ModelUser;
use Config;

/**
*
*/
class RankController extends Controller
{

	protected $collection = "rank";

	function __construct()
	{
		# code...
	}

	public function createRank ($data_user, $level, $coint) {

		$ModelRank = new ModelRank();

		$data_config = Config::get('database_config.rank');
		$data_user['_id'] = $data_user['_id'];
		$data_config['user_info'] 	= $data_user;
		$data_config['level'] 		= $level;
		$data_config['coint'] 		= $coint;
		$data_config['rank']		= $ModelRank->getCounter();
		$data_config['created_time'] = strtotime(\Carbon\Carbon::now()->toDateTimeString());
		$data_config['updateed_time'] = strtotime(\Carbon\Carbon::now()->toDateTimeString());

		$create_new = $ModelRank->create_new_rank($data_config);

		return $create_new;
	}

	public function getListRank ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']){

			$ModelRank = new ModelRank();
			$ModelUser = new ModelUser();
			$offset = $data_output_validate_param['response']['offset'];
			$user   = $data_output_validate_param['data_user'];
			unset($data_output_validate_param['data_user']);

			$data 	= $ModelRank->getRank($offset);


			$list_id = [];
			foreach ($data['response'] as $key => $value) {

				array_push($list_id, (string) $value['user_info']['_id']);
			}

			$ListUser = $ModelUser->getDataUserByListId($list_id)['response'];

			$count_data = COUNT($data['response']);

			$data_return = [];
			for ($i=0; $i < $count_data; $i++) {

			 	$user_id = (string) $data['response'][$i]['user_info']['_id'];

			 	foreach ($ListUser as $key => $value) {

			 	 	if((string)$value['_id'] == $user_id){

			 	 		$item = $data['response'][$i];
			 	 		$item['user_info'] =  $value;
			 	 		unset($item['user_info']['token']);

			 	 		array_push($data_return, $item);
			 	 		break;
			 	 	}
			 	 }
			 }
			$data_current_user = $this->getRankByUserId($user);

			$data_output_validate_param['response']['list_rank']    = $data_return;
			$data_output_validate_param['response']['current_user'] = $data_current_user['response'];
		}

		return $data_output_validate_param;
	}

	public function getRankByUserId ($user) {
		//var_dump($user);
		//exit();

		$ModelRank = new ModelRank();
		// dd($user);
		$data 	= $ModelRank->getRankByUserId((string) $user['_id']);
		$total_rank_lv_higher 		= $ModelRank->total_rank_lv_higher($data['response']['level']);
		$total_rank_coint_higher 	= $ModelRank->total_rank_coint_higher($data['response']['level'], $data['response']['coint']);
		$total_rank_same_rank 		= $ModelRank->total_rank_coint_higher($data['response']['level'], $data['response']['coint'], $data['response']['updated_time']);

		($total_rank_lv_higher['meta']['success'])? $total_rank_lv_higher = (int)$total_rank_lv_higher['response'] : $total_rank_lv_higher = 0;
		($total_rank_coint_higher['meta']['success'])? $total_rank_coint_higher = (int)$total_rank_coint_higher['response'] : $total_rank_coint_higher = 0;
		($total_rank_same_rank['meta']['success'])? $total_rank_same_rank = (int)$total_rank_same_rank['response'] : $total_rank_same_rank = 0;

		$data['response']['rank'] = $total_rank_lv_higher + $total_rank_coint_higher + $total_rank_same_rank + 1;
		unset($user['token']);
		$data['response']['user_info'] = $user;
		return $data;
	}

	public function getDataMaxRankSmallerThanCurentUser($level, $total_coint) {

		$ModelRank = new ModelRank();

		$data 	= $ModelRank->getDataMaxRankSmallerThanCurentUser($level, $total_coint);

		return $data;
	}

	public function updateReducedRankOfOtherUser($start, $end){

		$ModelRank = new ModelRank();

		$data 	= $ModelRank->updateReducedRankOfOtherUser($start, $end);

		return $data;
	}

	public function updateDataByUsersId ($users_id, $rank, $level, $coint) {

		$ModelRank = new ModelRank();

		$data 	= $ModelRank->updateDataByUsersId($users_id, $rank, $level, $coint);

		return $data;
	}

	public function updateInforUser($data) {

		$ModelRank = new ModelRank();

		$data_update = [

			'user_info' => $data
		];
		$data 	= $ModelRank->updateInforUser($data_update);

		return $data;
	}
}