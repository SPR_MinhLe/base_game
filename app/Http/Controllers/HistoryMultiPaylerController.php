<?php

namespace App\Http\Controllers;

use Input;
use App\Http\Models\HistoryMultiPayler as ModelHistoryMultiPayler;
use App\Http\Models\User as ModelUser;
use Spr\Base\Response\Response;
use Cache;

/**
*
*/
class HistoryMultiPaylerController extends Controller
{

	protected $collection = "history";

	function __construct()
	{
		# code...
	}

	public function getListHistoryByUserId ($data_output_validate_param) {

		if($data_output_validate_param['meta']['success']){

			$id_current_user = (string)$data_output_validate_param['data_user']['_id'];

			$ModelHistoryMultiPayler = new ModelHistoryMultiPayler();
			$ModelUser = new ModelUser();
			$dataHistory = $ModelHistoryMultiPayler->getDataHistory($id_current_user);
			$listDataHistory = [];

			$listPlayer = [];

			foreach ($dataHistory['response'] as $key => $value) {

				if($value['winer'] == $id_current_user){

					array_push($listPlayer, $value['loser']);
				}else {
					array_push($listPlayer, $value['winer']);
				}
			}

			$ListUser = $ModelUser->getDataUserByListId($listPlayer)['response'];

			foreach ($dataHistory['response'] as $key => $value) {

				$type = Cache::get('api-config')['status']['win_lose']['win'];

				$player = null;

				if($value['winer'] != $id_current_user){

					$type = Cache::get('api-config')['status']['win_lose']['lose'];
					$player = $value['winer'];

				}else {

					$player = $value['loser'];

				}

				foreach ($ListUser as $ListUser_key => $ListUser_value) {

					if((string)$ListUser_value['_id'] == $player) {

						$player = $ListUser_value;
						break;
					}
				}

				array_push($listDataHistory, [

					'type' 	=> $type,
					'player' => $player
				]);
			}
			$data_output_validate_param['response'] = [];
			$data_output_validate_param['response']['current_user'] = $data_output_validate_param['data_user'];
			$data_output_validate_param['response']['history'] = $listDataHistory;
			unset($data_output_validate_param['data_user']);
			unset($data_output_validate_param['response']['token']);
		}

		return $data_output_validate_param;
	}

}