<?php
namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
use Config;
/**
*
*/
class Item extends Moloquent
{

    protected $table = "item";

   public function getDataItem() {

        return HelperMongo::select($this->table);
   }

   public function getDataManager ($key_search, $limit, $sort, $sort_type){


        $where = [
            // [
            //     'fields' => 'level',
            //     'operator' => 'like',
            //     'value' => '%'. $key_search . '%',
            // ],
            [
                'fields' => 'deleted_at',
                'operator' => 'null',
                'value' => 'NULL',
            ]
        ];
        $order = [
            [
                'fields' => $sort,
                'operator'  => $sort_type
            ]
        ];
        $results = HelperMongo::select($this->table, $where , (int)$limit, null, Config::get('spr.system.type.query.paginate'), null, $order );
        return $results;
   	}

    public function updateData ($data, $where) {

        return HelperMongo::update_db($this->table, $data, $where);
    }
}