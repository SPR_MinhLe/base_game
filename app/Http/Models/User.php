<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spr\Base\Models\HelperMongo;
use Config;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "users";
    protected $counters = "counters";
    protected $field = "users";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDataManager ($key_search, $limit, $sort, $sort_type){


        $where = [

            [
                'fields' => 'deleted_at',
                'operator' => 'null',
                'value' => 'NULL',
            ]
        ];

        if($key_search != '')  {

            $new_where = [

                'fields' => 'email',
                'operator' => 'like',
                'value' => '%' . $key_search . '%',
            ];

            array_push($where, $new_where);
        }
        $order = [
            [
                'fields' => $sort,
                'operator'  => $sort_type
            ]
        ];
        $results = HelperMongo::select($this->table, $where , (int)$limit, null, Config::get('spr.system.type.query.paginate'), null, $order );
        return $results;
    }

    public function getDataById($id) {

        $where = [
            [
                'fields'    => '_id',
                'value'     => $id,
                'operator'  => '='
            ]
        ];

        return HelperMongo::select($this->table, $where);
    }

    public function getDataByEmail($email) {

        $where = [
            [
                'fields'    => 'email',
                'value'     => $email,
                'operator'  => '='
            ]
        ];

        return HelperMongo::select($this->table, $where);
    }

    public function getDataByGoogleId($google_id) {

        $where = [
            [
                'fields'    => 'google_id',
                'value'     => $google_id,
                'operator'  => '='
            ]
        ];

        return HelperMongo::select($this->table, $where);
    }

    public function registerNewAccount($data) {


        return HelperMongo::insert($this->table, $data);
    }

    public function getDataByFacebookId($facebook_id) {
        $where = [
            [
                'fields'    => 'facebook_id',
                'value'     => $facebook_id,
                'operator'  => '='
            ]
        ];

        return HelperMongo::select($this->table, $where);
    }

    public function getDataByToken($key, $secret) {
        $where = [
            [
                'fields'    => 'token.key',
                'value'     => $key,
                'operator'  => '='
            ],
            [
                'fields'    => 'token.secret',
                'value'     => $secret,
                'operator'  => '='
            ]
        ];

        return HelperMongo::select($this->table, $where, null, null, Config::get('spr.system.type.query.first'));
    }

    public function updateData($data) {

        $id = $data['_id'];

        unset($data['_id']);

        $where = [
            [
                'fields'    => '_id',
                'value'     => $id,
                'operator'  => '='
            ]
        ];
        return HelperMongo::update_db($this->table, $data, $where);
    }

    public function getCounter () {

        return HelperMongo::getNextSequence($this->counters, $this->table);
    }

    public function updateCointEndGame($users_id, $coint){

        $where = [
            [
                'fields'    => '_id',
                'value'     => $users_id,
                'operator'  => '='
            ]
        ];
        $data_update = [

            'fields' => 'current_coint',
            'value' => $coint
        ];
        return HelperMongo::incrementData($this->table, $where, $data_update);
    }

    public function updateCointPurchase($users_id, $coint){

        $where = [
            [
                'fields'    => '_id',
                'value'     => $users_id,
                'operator'  => '='
            ]
        ];
        $data_update = [

            'fields' => 'current_coint',
            'value' => $coint
        ];
        return HelperMongo::incrementData($this->table, $where, $data_update);
    }

    public function updateCointUsedItem($users_id, $coint){

        $where = [
            [
                'fields'    => '_id',
                'value'     => $users_id,
                'operator'  => '='
            ]
        ];
        $data_update = [

            'fields' => 'current_coint',
            'value' => $coint
        ];
        return HelperMongo::decrementData($this->table, $where, $data_update);
    }

    public function getDataUserByListId($list_id){

        $where = [
            [
                'fields'    => '_id',
                'value'     => $list_id,
                'operator'  => 'in'
            ]
        ];

        return HelperMongo::select($this->table, $where);
    }
}
