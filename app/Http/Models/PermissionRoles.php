<?php

namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;

/**
*
*/
class PermissionRoles extends Moloquent
{

	protected $table = "permission_roles";

	public static function insertData($table, $data, $where) {

		$results = HelperMongo::insertGetId($table, $data, $where);

		return $results;
	}

	public static function selectData($table, $where) {

		$results = HelperMongo::select($table, $where);

		return $results;
	}

	public static function updateData($table, $data, $where) {

		$results = HelperMongo::update_db($table, $data, $where);

		return $results;
	}
}