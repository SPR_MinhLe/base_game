<?php
namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
use Config;
/**
*
*/
class HistoryMultiPayler extends Moloquent
{

    protected $table = "history_multi_player";


    public function getDataHistory($users_id){

        $results = Response::response();

        try {

            $query = DB::collection($this->table)
                    ->where('winer', $users_id)
                    ->orWhere('loser', $users_id)
                    ->get();

            $results['response'] = $query;

        }catch(PDOException $e) {

            $results['meta']['success'] = false;
            $results['meta']['code'] = 401;
            $results['meta']['msg'] = $e->getMessage();
        }

        return $results;
    }

}