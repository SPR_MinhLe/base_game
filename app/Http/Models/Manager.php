<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use App\Http\Controllers\Auth\User as Authenticatable;
use Spr\Base\Models\HelperMongo;
use Spr\Base\Response\Response;
use Config;
use DB;

class Manager extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'managers';
    protected $guard = 'managers';

    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function insertData($data, $where = array()) {

        $results = HelperMongo::insertGetId('customers', $data, $where);
        return $results;
    }

    public static function insertGetId($data) {

        $results = HelperMongo::insertGetId("customers", $data);
        return $results;
    }

    public function updateData($data,$where = array()){
        $results =  HelperMongo::update_db($this->table,$data,$where);

        return $results;
    }

    public function selectData($where = array()){

        $results =  HelperMongo::select($this->table,$where);

        return $results;
    }
}
