<?php
namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
use Config;
/**
*
*/
class HistoryIAP extends Moloquent
{

    protected $table = "history_iap"; 


    public function createNewLog($store, $product, $transaction_id, $purchase_token, $user) {

        $data_config = Config::get('database_config.history_iap');

        $data_config["store"]               = $store;
        $data_config["product"]             = $product;
        $data_config["transaction_id"]      = $transaction_id;
        $data_config["purchase_token"]      = $purchase_token;
        $data_config["user"]                = $user;
        $data_config["created_time"] = strtotime(\Carbon\Carbon::now()->toDateTimeString());

        return HelperMongo::insert($this->table, $data_config);
    }

    public function getDataHistoryByTransactionId($transaction_id){

        $where = [
            [
                'fields' => 'transaction_id',
                'value'  => $transaction_id,
                'operator' => '='
            ]
        ];

        return HelperMongo::select($this->table, $where  );
    }

    public function getDataManager ($key_search, $limit, $sort, $sort_type){


        $where = [
            [
                'fields' => 'deleted_at',
                'operator' => 'null',
                'value' => 'NULL',
            ]
        ];

        if($key_search != '') {

            array_push($where, [

                'fields' => 'user.email',
                'operator' => 'like',
                'value' => '%'. $key_search . '%',
            ]);
        }

        $order = [
            [
                'fields' => $sort,
                'operator'  => $sort_type
            ]
        ];
        $results = HelperMongo::select($this->table, $where , (int)$limit, null, Config::get('spr.system.type.query.paginate'), null, $order );
        return $results;
    }
}