<?php
namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
use Config;
use Cache;
/**
*
*/
class LogSingleMode extends Moloquent
{

    protected $table = "log_single_mode";

    public function createNewLog($game_id, $data_user, $status, $activity_code, $level, $db_target , $id_target = null, $amount = 0, $price = 0) {

   		$data_config = Config::get('database_config.log_single_mode');

	    $data_config["game_id"] 	= $game_id;
	    $data_config["user_info"]	= $data_user;
	    $data_config["activity"] 	= $activity_code;
	    $data_config["db_target"] 	= $db_target;
	    $data_config["id_target"] 	= $id_target;
	    $data_config["amount"]		= $amount;
	    $data_config["price"]		= $price;
	    $data_config["status"] 		= $status;
        $data_config["level"]       = $level;
	    $data_config["created_time"] = strtotime(\Carbon\Carbon::now()->toDateTimeString());

        return HelperMongo::insert($this->table, $data_config);
    }

    public function countDataGame ($game_id, $level, $user_id, $activity = null, $target_id = null) {

    	$where = [
    		[
    			'fields' => 'game_id',
    			'value'	 => $game_id,
    			'operator' => '='
    		],
    		[
    			'fields' => 'user_info._id',
    			'value'	 => $user_id,
    			'operator' => '='
    		],
            [
                'fields' => 'level',
                'value'  => $level,
                'operator' => '='
            ]

    	];

    	if($activity != null) {

    		array_push($where, [

    			'fields' => 'activity',
    			'value'	 => $activity,
    			'operator' => '='
    		]);
    	}

    	if($target_id != null) {

    		array_push($where, [

    			'fields' => 'id_target',
    			'value'	 => $target_id,
    			'operator' => '='
    		]);
    	}

    	return HelperMongo::select($this->table, $where, null, null, Config::get('spr.system.type.query.count'));
    }

    public function getAllCointOnGameId($game_id, $data_user, $activity) {

        $where = [
            [
                'fields' => 'game_id',
                'value'  => $game_id,
                'operator' => '='
            ],
            [
                'fields' => 'user_info._id',
                'value'  => (string)$data_user['_id'],
                'operator' => '='
            ],
            [
                'fields' => 'activity',
                'value'  => $activity,
                'operator' => '='
            ]

        ];

        $groupBy = [
            ['fields' => 'game_id']
        ];

        return HelperMongo::select($this->table, $where, null, null, Config::get('spr.system.type.query.sum'), 'price');
    }


}