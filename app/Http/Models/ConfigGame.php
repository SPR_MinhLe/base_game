<?php
namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
/**
*
*/
class ConfigGame extends Moloquent
{

    protected $table = "config";

   public function getDataConfig() {

        return HelperMongo::select($this->table);
   }

   public function updateData ($data, $where) {

        return HelperMongo::update_db($this->table, $data, $where);
    }
}