<?php
namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
use Config;
/**
*
*/
class Rank extends Moloquent
{

    protected $table = "rank";
    protected $counters = "counters";

    public function create_new_rank ($data) {

        return HelperMongo::insert($this->table, $data);
    }

    public function getCounter () {

        return HelperMongo::getNextSequence($this->counters, $this->table);
    }

    public function getRank ($offset) {


        $order = [
            [
                'fields' => 'level' ,
                'operator' => 'DESC'
            ],
            [
                'fields' => 'coint' ,
                'operator' => 'DESC'
            ],
            [
                'fields' => 'updated_time' ,
                'operator' => 'ASC'
            ]

        ];

        $limit = Config::get('api.system.default_variable.count-item-response.rank');
        return HelperMongo::select($this->table, [], $limit, $offset, null, null, $order);
    }

    public function total_rank_lv_higher ($level) {

        $where = [
            [
                'fields'    => 'level',
                'value'     => $level,
                'operator'  => '>'
            ]
        ];

        return HelperMongo::select($this->table, $where, null, null, Config::get('spr.system.type.query.count'));
    }

    public function total_rank_coint_higher ($level, $coint, $updated_time = null) {


        if($updated_time == null ){

            $where = [
                [
                    'fields'    => 'level',
                    'value'     => $level,
                    'operator'  => '='
                ],
                [
                    'fields'    => 'coint',
                    'value'     => $coint,
                    'operator'  => '>'
                ]
            ];

        }else {

            $where = [
                [
                    'fields'    => 'level',
                    'value'     => $level,
                    'operator'  => '='
                ],
                [
                    'fields'    => 'coint',
                    'value'     => $coint,
                    'operator'  => '='
                ],
                [
                    'fields'    => 'updated_time',
                    'value'     => $updated_time,
                    'operator'  => '<'
                ],
            ];

        }

        return HelperMongo::select($this->table, $where, null, null, Config::get('spr.system.type.query.count'));

    }


    public function getRankByUserId ($user_id) {
        $where = [
            [
                'fields'    => 'user_info._id',
                'value'     => (string)$user_id,
                'operator'  => '='
            ]
        ];

        return HelperMongo::select($this->table, $where, null, null, Config::get('spr.system.type.query.first'));
    }

    public function getDataMaxRankSmallerThanCurentUser($level, $total_coint){

        $where = [
            [
                'fields'    => 'level',
                'value'     => $level,
                'operator'  => '<='
            ],
            [
                'fields'    => 'coint',
                'value'     => $total_coint,
                'operator'  => '<='
            ]
        ];
        return HelperMongo::select($this->table, $where, null, null, Config::get('spr.system.type.query.min'),'rank');
    }

    public function updateReducedRankOfOtherUser ($start, $end) {

        $where = [
            [
                'fields'    => 'rank',
                'value'     => $start,
                'operator'  => '>='
            ],
            [
                'fields'    => 'rank',
                'value'     => $end,
                'operator'  => '<'
            ]
        ];
        $data_update = [

            'fields' => 'rank',
            'value' => 1
        ];
        return HelperMongo::incrementData($this->table, $where, $data_update);
    }

    public function updateDataByUsersId ($users_id, $rank, $level, $coint) {

        $where = [
            [
                'fields'    => 'user_info._id',
                'value'     => $users_id,
                'operator'  => '='
            ]
        ];
        $data_update = [

            'rank' => $rank,
            'level' => $level,
            'coint' => $coint,
            'updated_time' => strtotime(\Carbon\Carbon::now()->toDateTimeString())
       
        ];
        return HelperMongo::update_db($this->table, $data_update, $where);
    }

    public function updateInforUser($data) {

        $where = [
            [
                'fields'    => 'user_info._id',
                'value'     => $data['user_info']['_id'],
                'operator'  => '='
            ]
        ];
        // unset($data['user_info']['_id']);
        return HelperMongo::update_db($this->table, $data, $where);
    }
}