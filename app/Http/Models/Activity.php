<?php
namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
use Config;
/**
*
*/
class Activity extends Moloquent
{

    protected $table = "activity";

   public function getDataActivity() {

        return HelperMongo::select($this->table);
   }
}