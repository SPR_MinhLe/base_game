<?php
namespace App\Http\Models;

use DB;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
use Config;
/**
*
*/
class HightCoint extends Moloquent
{

    protected $table = "hight_coint";

    public function updateData ($data) {

        $id = $data['_id'];
        unset($data['_id']);

        $where = [
            [
                'fields'    => '_id',
                'value'     => $id,
                'operator'  => '='
            ]
        ];
        return HelperMongo::update_db($this->table, $data, $where);
    }

    public function createNewHightCoint($data) {

        return HelperMongo::insert($this->table, $data);
    }

    public function getDataByUserId($users_id){
        $where = [
            [
                'fields'    => 'users_id',
                'value'     => $users_id,
                'operator'  => '='
            ]
        ];

        return HelperMongo::select($this->table, $where);
    }

    public function getDataByUserIdAndLevel($id_user, $level) {
        $where = [
            [
                'fields'    => 'users_id',
                'value'     => $id_user,
                'operator'  => '='
            ],
            [
                'fields'    => 'level',
                'value'     => $level,
                'operator'  => '='
            ]
        ];

        return HelperMongo::select($this->table, $where);
    }


    public function getDataRankUsers () {


        $order = [
            [
                'fields' => 'level' ,
                'operator' => 'DESC'
            ],
            [
                'fields' => 'coint' ,
                'operator' => 'DESC'
            ]
        ];

        $groupBy = [
            [ 'fields' => 'users_id' ]
        ];

        $listSelect = ['user_info', 'users_id', 'level', 'coint'];
        return HelperMongo::select($this->table, [], null, null, null, null, $order, $groupBy, $listSelect);
    }
}