<?php
namespace App\Http\Validates;

use Config;
use Spr\Base\Response\Response;
use Spr\Base\Validates\Helper as ValidateHelper;
use App\Http\Models\User as ModelUser;
use Lang;
use Hash;
use Validator;
use Auth;

class ValidateAuthApi {

    public function __construct () {

    }

    public static function validate($data_output_get_param) {

    	$data_out_put_validate_base = ValidateHelper::baseValidate($data_output_get_param);

    	if($data_out_put_validate_base['meta']['success']) {

    		$ModelUser = new ModelUser();
    		$token_key 		= $data_out_put_validate_base['response']['token_key'];
    		$token_secret 	= $data_out_put_validate_base['response']['token_secret'];

    		$data_user = $ModelUser->getDataByToken($token_key, $token_secret);
    		
    		if($data_user['meta']['success'] && $data_user['response'] != null) {

    			$data_out_put_validate_base['data_user'] =  $data_user['response'];
    		}else {

    			$data_out_put_validate_base['data_user'] = [];
    			$data_out_put_validate_base['meta']['success'] 	= false;
    			$data_out_put_validate_base['meta']['code'] 	= 401;
    			$data_out_put_validate_base['meta']['msg'] 		= ["auth" => Lang::get('message.api.error.000002')];
    			$data_out_put_validate_base['response']		 	= [];
    		}
    	}
        
    	return $data_out_put_validate_base;
    }

}