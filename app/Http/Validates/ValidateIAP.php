<?php
namespace App\Http\Validates;

use Config;
use Spr\Base\Response\Response;
use Spr\Base\Validates\Helper as ValidateHelper;
use App\Http\Validates\ValidateAuthApi;
use App\Http\Models\User as ModelUser;
use App\Http\Models\HistoryIAP as ModelHistoryIAP;
use Lang;
use Hash;
use Validator;
use Auth;
use Cache;

class ValidateIAP {

    public function __construct () {

    }

    public function validateTransaction($data_output_get_param) {

    	$data_out_put_validate_base = ValidateAuthApi::validate($data_output_get_param);

    	if($data_out_put_validate_base['meta']['success']) {

            $ModelHistoryIAP = new ModelHistoryIAP();
            $transaction_id  = $data_out_put_validate_base['response']['transaction_id'];
            $product_id      = $data_out_put_validate_base['response']['product_id'];
            $list_product    = Cache::get('product') ;

            if( isset( $list_product[ $product_id ] ) ) {

                $list_data_history = $ModelHistoryIAP->getDataHistoryByTransactionId( $transaction_id );

                if($list_data_history['meta']['success'] && COUNT( $list_data_history['response'] ) > 0) {

                    $data_out_put_validate_base['meta']['success']  = false;
                    $data_out_put_validate_base['meta']['code']     = 500;
                    $data_out_put_validate_base['meta']['msg']      = ['error' => Lang::get('service/google.error.0003')];
                    $data_out_put_validate_base['response']         = [];
                    unset($data_out_put_validate_base['data_user']);
                }
            }else {

                $data_out_put_validate_base['meta']['success']  = false;
                $data_out_put_validate_base['meta']['code']     = 500;
                $data_out_put_validate_base['meta']['msg']      = ['error' => Lang::get('service/google.error.0002')];
                $data_out_put_validate_base['response']         = [];
                unset($data_out_put_validate_base['data_user']);
            }

    	}

    	return $data_out_put_validate_base;
    }

    public function validateTransactionAppStore($data_output_get_param) {

        $data_out_put_validate_base = ValidateAuthApi::validate($data_output_get_param);

        if($data_out_put_validate_base['meta']['success']) {

            $ModelHistoryIAP = new ModelHistoryIAP();
            $transaction_id  = $data_out_put_validate_base['response']['transaction_id'];
            // $product_id      = $data_out_put_validate_base['response']['product_id'];
            $list_product    = Cache::get('product') ;

            // if( isset( $list_product[ $product_id ] ) ) {

            $list_data_history = $ModelHistoryIAP->getDataHistoryByTransactionId( $transaction_id );

            if($list_data_history['meta']['success'] && COUNT( $list_data_history['response'] ) > 0) {

                $data_out_put_validate_base['meta']['success']  = false;
                $data_out_put_validate_base['meta']['code']     = 500;
                $data_out_put_validate_base['meta']['msg']      = ['error' => Lang::get('service/google.error.0003')];
                $data_out_put_validate_base['response']         = [];
                unset($data_out_put_validate_base['data_user']);
            }
            // }else {

            //     $data_out_put_validate_base['meta']['success']  = false;
            //     $data_out_put_validate_base['meta']['code']     = 500;
            //     $data_out_put_validate_base['meta']['msg']      = ['error' => Lang::get('service/google.error.0002')];
            //     $data_out_put_validate_base['response']         = [];
            //     unset($data_out_put_validate_base['data_user']);
            // }

        }

        return $data_out_put_validate_base;
    }
}