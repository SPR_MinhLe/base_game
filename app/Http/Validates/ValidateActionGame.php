<?php
namespace App\Http\Validates;

use Config;
use Spr\Base\Response\Response;
use Spr\Base\Validates\Helper as ValidateHelper;
use App\Http\Validates\ValidateAuthApi;
use App\Http\Models\User as ModelUser;
use App\Http\Models\LogSingleMode as ModelLogSingleMode;
use Lang;
use Hash;
use Validator;
use Auth;
use Cache;

class ValidateActionGame {

    public function __construct () {

    }

    public function validateStartGame($data_output_get_param) {

    	$data_out_put_validate_base = ValidateAuthApi::validate($data_output_get_param);

    	if($data_out_put_validate_base['meta']['success']) {

            $ModelLogSingleMode = new ModelLogSingleMode();
            $game_id        = $data_out_put_validate_base['response']['game_id'];
            $level_id       = $data_out_put_validate_base['response']['level_id'];
            $config_level   = Cache::get('api-level');

            if(isset($config_level[$level_id])) {

                $level           = $config_level[$level_id]['level'];
                $data_user       = $data_out_put_validate_base['data_user'];
                $count_data_game = $ModelLogSingleMode->countDataGame($game_id, $level, $data_user['_id']);
                if($count_data_game['meta']['success'] && $count_data_game['response'] > 0) {

                    $data_out_put_validate_base['meta']['success'] = false;
                    $data_out_put_validate_base['meta']['code'] = 501;
                    $data_out_put_validate_base['meta']['msg'] = ['game' => Lang::get('message.api.error.000006')];
                    $data_out_put_validate_base['response'] = [];
                    unset($data_out_put_validate_base['data_user']);
                }
            }else {

                $data_out_put_validate_base['meta']['success'] = false;
                $data_out_put_validate_base['meta']['code'] = '000004';
                $data_out_put_validate_base['meta']['msg'] = ['level' => Lang::get('message.api.error.000004')];
                $data_out_put_validate_base['response'] = [];
                unset($data_out_put_validate_base['data_user']);
            }

    	}

    	return $data_out_put_validate_base;
    }



    public function validateEndGame ($data_output_get_param) {

        $data_out_put_validate_base = ValidateAuthApi::validate($data_output_get_param);

        if($data_out_put_validate_base['meta']['success']) {

            $ModelLogSingleMode = new ModelLogSingleMode();
            $game_id        = $data_out_put_validate_base['response']['game_id'];
            $data_user      = $data_out_put_validate_base['data_user'];
            $level_id       = $data_out_put_validate_base['response']['level_id'];
            $config_level   = Cache::get('api-level');

            if(isset($config_level[$level_id])) {

                $level           = $config_level[$level_id]['level'];
                $count_data_game = $ModelLogSingleMode->countDataGame($game_id, $level, $data_user['_id']);
                if($count_data_game['meta']['success'] && $count_data_game['response'] > 0) {

                    $check_game_end = $ModelLogSingleMode->countDataGame($game_id, $level, $data_user['_id'], Cache::get('api-activity')['end-game']['code']);

                    if(!$check_game_end['meta']['success'] || $check_game_end['response'] > 0) {

                        $data_out_put_validate_base['meta']['success']  = false;
                        $data_out_put_validate_base['meta']['code']     = '000005';
                        $data_out_put_validate_base['meta']['msg']      = ['game' => Lang::get('message.api.error.000005')];
                        unset($data_out_put_validate_base['data_user']);
                        $data_out_put_validate_base['response']         = [];
                    }
                }else {

                    $data_out_put_validate_base['meta']['success']  = false;
                    $data_out_put_validate_base['meta']['code']     = '000004';
                    $data_out_put_validate_base['meta']['msg']      = ['game' => Lang::get('message.api.error.000004')];
                    unset($data_out_put_validate_base['data_user']);
                    $data_out_put_validate_base['response']         = [];
                }
            }else {

                $data_out_put_validate_base['meta']['success']  = false;
                $data_out_put_validate_base['meta']['code']     = '000004';
                $data_out_put_validate_base['meta']['msg']      = ['level' => Lang::get('message.api.error.000004')];
                $data_out_put_validate_base['response']         = [];
                unset($data_out_put_validate_base['data_user']);
            }
        }

        return $data_out_put_validate_base;
    }


    public function validateUseItem ($data_output_get_param) {

        $validateUseItem = $this->validateEndGame($data_output_get_param);

        if($validateUseItem['meta']['success']) {

            $id_item = $validateUseItem['response']['item_id'];
            $list_item = Cache::get('item');

            if(isset($list_item[$id_item])) {

                $ModelLogSingleMode = new ModelLogSingleMode();
                $game_id        = $validateUseItem['response']['game_id'];
                $data_user      = $validateUseItem['data_user'];
                $level_id       = $validateUseItem['response']['level_id'];
                $price_Base     = $list_item[$id_item]['price_Base'];
                $activity       = Cache::get('api-activity')['use-item']['code'];
                $level          = Cache::get('api-level')[$level_id]['level'];

                $data_total_count_item_used = $ModelLogSingleMode->countDataGame($game_id, $level, $data_user['_id'], $activity, $id_item);
                if($data_total_count_item_used['meta']['success']) {
                    $total_count_item_used = $data_total_count_item_used['response'] + 1;

                    $count_use_item         = ($total_count_item_used - 1) * 2;
                    $count_next_use_item    = $total_count_item_used * 2;
                    if($count_use_item == 0) $count_use_item = 1 ;
                    $price                  = $count_use_item * $price_Base;
                    $price_next_use_item    = $count_next_use_item * $price_Base;

                    if($data_user['current_coint'] < $price){

                        $validateUseItem['meta']['success'] = false;
                        $validateUseItem['meta']['code'] = '000009';
                        $validateUseItem['meta']['msg'] = ['item' => Lang::get('message.api.error.000009')];
                        unset($validateUseItem['data_user']);
                        $validateUseItem['response'] = [];
                    }else {

                        $validateUseItem['response']['price']               = $price;
                        $validateUseItem['response']['price_next_use_item'] = $price_next_use_item;
                        $validateUseItem['response']['total_use']           = $total_count_item_used;
                    }

                }else {

                    $validateUseItem['meta']['success'] = false;
                    $validateUseItem['meta']['code'] = '000008';
                    $validateUseItem['meta']['msg'] = ['item' => Lang::get('message.api.error.000008')];
                    unset($validateUseItem['data_user']);
                    $validateUseItem['response'] = [];
                }
            }else {

                $validateUseItem['meta']['success'] = false;
                $validateUseItem['meta']['code'] = '000007';
                $validateUseItem['meta']['msg'] = ['item' => Lang::get('message.api.error.000007')];
                unset($validateUseItem['data_user']);
                $validateUseItem['response'] = [];
            }
        }

        return $validateUseItem;
    }

    public function validateGetCoint($data_output_get_param) {

        $validateUseItem = $this->validateEndGame($data_output_get_param);

        if($validateUseItem['meta']['success']) {

            $game_play_id = $validateUseItem['response']['game_play_id'];
            $config_game_play = Cache::get('api-game-play');

            if(!isset($config_game_play[$game_play_id])) {

                $validateUseItem['meta']['code'] = '000004';
                $validateUseItem['meta']['success'] = false;
                $validateUseItem['meta']['msg'] = ['game_play_id' => Lang::get('message.api.error.000004')];
                $validateUseItem['response'] = [];
                unset($validateUseItem['data_user']);
            }
        }

        return $validateUseItem;
    }

    public function validateRewardMoreCoin($data_output_get_param) {

        $data_out_put_validate_base = ValidateAuthApi::validate($data_output_get_param);

        if($data_out_put_validate_base['meta']['success']) {

            $ModelLogSingleMode = new ModelLogSingleMode();
            $game_id        = $data_out_put_validate_base['response']['game_id'];
            $data_user      = $data_out_put_validate_base['data_user'];
            $reward_type    = $data_out_put_validate_base['response']['reward_type'];
            $level_id       = $data_out_put_validate_base['response']['level_id'];
            $activity       =  ( Cache::get('api-config')['type']['reward']['video'] == $reward_type)? Cache::get('api-activity')['reward-video']['code'] : Cache::get('api-activity')['share-facebook']['code'];
            $config_level   = Cache::get('api-level');
            // dd($config_level);
            if(isset($config_level[$level_id])) {

                $level           = $config_level[$level_id]['level'];
                $count_data_game = $ModelLogSingleMode->countDataGame($game_id, $level, $data_user['_id']);
                if($count_data_game['meta']['success'] && $count_data_game['response'] > 0) {

                    $check_game_reward = $ModelLogSingleMode->countDataGame($game_id, $level, $data_user['_id'], $activity);

                    if(!$check_game_reward['meta']['success'] || $check_game_reward['response'] > 0) {

                        $data_out_put_validate_base['meta']['success']  = false;
                        $data_out_put_validate_base['meta']['code']     = '000010';
                        $data_out_put_validate_base['meta']['msg']      = ['action' => Lang::get('message.api.error.000010')];
                        $data_out_put_validate_base['response']         = [];
                        unset($data_out_put_validate_base['data_user']);
                    }
                }else {

                    $data_out_put_validate_base['meta']['success']  = false;
                    $data_out_put_validate_base['meta']['code']     = '000004';
                    $data_out_put_validate_base['meta']['msg']      = ['game' => Lang::get('message.api.error.000004')];
                    $data_out_put_validate_base['response']         = [];
                    unset($data_out_put_validate_base['data_user']);
                }
            }else {

                $data_out_put_validate_base['meta']['success']  = false;
                $data_out_put_validate_base['meta']['code']     = '000004';
                $data_out_put_validate_base['meta']['msg']      = ['level' => Lang::get('message.api.error.000004')];
                $data_out_put_validate_base['response']         = [];
                unset($data_out_put_validate_base['data_user']);
            }
        }

        return $data_out_put_validate_base;
    }
}