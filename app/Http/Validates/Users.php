<?php
namespace App\Http\Validates;

use Config;
use Spr\Base\Response\Response;
use Spr\Base\Validates\Helper as ValidateHelper;
use App\Http\Validates\ValidateAuthApi;
use App\Http\Models\User as ModelUser;
use Lang;
use Hash;
use Validator;
use Auth;
use Cache;

class Users {

    public function __construct () {

    }

    public function validateBlock($data_output_get_param) {

    	$data_out_put_validate_base = ValidateHelper::baseValidate($data_output_get_param);

    	if($data_out_put_validate_base['meta']['success']) {

            $ModelUser = new ModelUser();
            $_id             = (string)$data_out_put_validate_base['response']['_id'];

            $data_user = $ModelUser->getDataById( $_id );

            if(!$data_user['meta']['success'] || COUNT( $data_user['response'] ) == 0) {

                $data_out_put_validate_base['meta']['success']  = false;
                $data_out_put_validate_base['meta']['code']     = 500;
                $data_out_put_validate_base['meta']['msg']      = ['error' => Lang::get('message.web.error.000006')];
                $data_out_put_validate_base['response']         = [];
                unset($data_out_put_validate_base['data_user']);
            }
    	}

    	return $data_out_put_validate_base;
    }
}