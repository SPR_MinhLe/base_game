<?php
namespace App\Http\Validates;

use Config;
use Spr\Base\Response\Response;
use Spr\Base\Validates\Helper as ValidateHelper;
use App\Http\Validates\ValidateAuthApi;
use App\Http\Models\User as ModelUser;
use App\Http\Models\LogSingleMode as ModelLogSingleMode;
use Lang;
use Hash;
use Validator;
use Auth;
use Cache;

class ValidateInvitedFriendsFacebook {

    public function __construct () {

    }

    public function invitedFriends($data_output_get_param) {

        $data_out_put_validate_base = ValidateAuthApi::validate($data_output_get_param);

        if($data_out_put_validate_base['meta']['success']) {

        	$list_friends = json_decode($data_out_put_validate_base['response']['friends_list']);
        	$data_user = $data_out_put_validate_base['data_user'];
        	$data_access = [];
        	$data_old = [
                'created_time' => strtotime(\Carbon\Carbon::now()->toDateTimeString()),
                'data'          => []
            ];

            if($list_friends == null) {
                $data_out_put_validate_base['meta']['success']  = false;
                $data_out_put_validate_base['meta']['msg']      = ['invite' => Lang::get('message.api.error.000011')];
                $data_out_put_validate_base['meta']['code']     = "000011";
                $data_out_put_validate_base['response']         = [];
                unset($data_out_put_validate_base['data_user']);
                return $data_out_put_validate_base;
            }

        	if(Cache::has('list_friends_' . (string)$data_user['_id'])){

        		$data_old = Cache::get('list_friends_' . (string)$data_user['_id']);

                $time_update = $data_old['created_time'];
                $day_update = date('d', $time_update);

                $now = strtotime(\Carbon\Carbon::now()->toDateTimeString());
                $day_now = date('d', $now);

                if($day_now == $day_update) {

                    foreach ($list_friends as $key => $value) {

            			if(!in_array($value, $data_old['data'])){

            				array_push($data_access, $value);
            			}
            		}
                }else {
                    $data_old['data'] = [];
                    $data_access = $list_friends;
                }
        	}else {

        		$data_access = $list_friends;
        	}

        	if(empty($data_access)) {

        		$data_out_put_validate_base['meta']['success']  = false;
        		$data_out_put_validate_base['meta']['msg']  	= ['invite' => Lang::get('message.api.error.000011')];
        		$data_out_put_validate_base['meta']['code']  	= "000011";
        		$data_out_put_validate_base['response'] 		= [];
        		unset($data_out_put_validate_base['data_user']);

        	}else {

        		$data_out_put_validate_base['response'] = $data_access;
        		$data_cache = array_merge($data_old['data'],$data_access);
                $data = [
                    'created_time' => strtotime(\Carbon\Carbon::now()->toDateTimeString()),
                    'data'  => $data_cache
                ];

                Cache::forget('list_friends_' . (string)$data_user['_id']);
                Cache::forever('list_friends_' . (string)$data_user['_id'], $data);
        	}
        }

        return $data_out_put_validate_base;
    }
}