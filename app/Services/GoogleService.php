<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Spr\Base\Response\Response;

class GoogleService {

    protected $client;

    protected $service;

    function __construct() {

        putenv('GOOGLE_APPLICATION_CREDENTIALS='.base_path() . '/resources/assets/PhaGachOnlineNew-4ac45f86b8dc.json'  );

        $this->client = new \Google_Client();
        $this->client->useApplicationDefaultCredentials();

        $this->client->addScope('https://www.googleapis.com/auth/androidpublisher');
        $this->service = new \Google_Service_AndroidPublisher($this->client);
    }

    public function get($calendarId)
    {
        $results = $this->service->calendars->get($calendarId);
        dd($results);
    }


    public function getPurchasesSubscriptions($productId, $token)
    {
        $results = Response::response();
        $packageName = Config::get('google.packageName');

        try {

            $subscription = $this->service->purchases_products->get($packageName, $productId, $token);
            $results[ 'response' ] = $subscription;

        } catch (Google_Auth_Exception $e) {
            // if the call to Google fails, throw an exception
            $results['meta']['success'] = false;
            $results['meta']['code'] = 500;
            $results['meta']['msg'] = [Lang::get('service/google.error.0001')];
        }

        return $results;
    }
}















