<?php
namespace Spr\Base\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use Spr\Base\Response\Response;
use DB;
use Config;

class Roles extends Moloquent{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'roles';
	public $timestamps = false;


	public function __construct(){


		// $this->now = \Carbon\Carbon::now()->toDateTimeString();
	}

	public  function getAllData() {

		$where = [];


		$results = HelperMongo::select($this->table, $where );

		return $results;
	}

	public function getDataManage ($limit, $sort, $sort_type) {


		$where = [];
		$order = [
			[
				'fields' => $sort,
				'operator'	=> $sort_type
			]
		];
		$results = HelperMongo::select($this->table, $where , (int)$limit, null, Config::get('spr.system.type.query.paginate'), null, $order );
		return $results;
	}

}
