<?php
namespace Spr\Base\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Spr\Base\Response\Response;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use DB;

class PermissionRoles extends Moloquent{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'permission_roles';
	public $timestamps = false;


	public function __construct(){


		// $this->now = \Carbon\Carbon::now()->toDateTimeString();
	}

	public function getAllDataRoles () {

		$where = array();
		$results = HelperMongo::select($this->table, $where);
		return $results;
	}

	public function getDataByRolesId ($roles_id) {

		$where = array(
			array(
				'fields' => 'roles_id',
				'operator' => '=',
				'value' => $roles_id
			)
		);
		$results = HelperMongo::select($this->table, $where);
		return $results;
	}

}
