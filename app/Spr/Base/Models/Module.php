<?php
namespace Spr\Base\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Spr\Base\Response\Response;
use Spr\Base\Models\HelperMongo;
use Moloquent;
use DB;
class Module extends Moloquent{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'modules';
	public $timestamps = false;


	public function __construct(){


		// $this->now = \Carbon\Carbon::now()->toDateTimeString();
	}

	public function getAllData () {

		$results = HelperMongo::select($this->table, array());
		return $results;
	}

}
