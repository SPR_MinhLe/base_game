<?php
// Đây là file cung cấp data cho các file được include mà route ko thể tác động
namespace Spr\Base\ViewComposers;

use Illuminate\Contracts\View\View;
// use Illuminate\Users\Repository as UserRepository;
use App\Http\Models\User as  ModelsUser;
// use App\Http\Models\Message as ModelMessage;
use Auth;
use Cache;
use Config;
use Lang;
class PageInformation
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
    	$data = array(

	    	'hor-menu' => [
	    			[
	    				'id'	=> '',
	    				'use' 	=> true,
			    		'name' 	=> Lang::get('menu.dashboard'),
			    		'slug'	=> 'dashboard',
			    		'url'	=> 'dashboard',
			    		'icon'	=> 'icon-home',
			    		'route' => 'auth-get-dashboard',
			    		'sub'	=> ''
			    	],
			    	// [
	    			// 	'id'	=> '',
	    			// 	'use' 	=> true,
			    	// 	'name' 	=> Lang::get('menu.roles'),
			    	// 	'slug'	=> 'manager-roles',
			    	// 	'url'	=> 'roles',
			    	// 	'icon'	=> 'icon-shield',
			    	// 	'route' => 'auth-get-permission-roles',
			    	// 	'sub'	=> ''
			    	// ],
			    	[
	    				'id'	=> '',
	    				'use' 	=> true,
			    		'name' 	=> Lang::get('menu.users'),
			    		'slug'	=> 'manager-users',
			    		'url'	=> 'users',
			    		'icon'	=> 'icon-puzzle',
			    		'route' => 'auth-get-users',
			    		'sub'	=> ''
			    	],
			    	[
	    				'id'	=> '',
	    				'use' 	=> true,
			    		'name' 	=> Lang::get('menu.items'),
			    		'slug'	=> 'manager-items',
			    		'url'	=> 'items',
			    		'icon'	=> 'icon-puzzle',
			    		'route' => 'auth-get-items',
			    		'sub'	=> ''
			    	],
			    	[
	    				'id'	=> '',
	    				'use' 	=> true,
			    		'name' 	=> Lang::get('menu.levels'),
			    		'slug'	=> 'manager-levels',
			    		'url'	=> 'levels',
			    		'icon'	=> 'fa fa-level-up',
			    		'route' => 'auth-get-levels',
			    		'sub'	=> ''
			    	],
			    	[
	    				'id'	=> '',
	    				'use' 	=> true,
			    		'name' 	=> Lang::get('menu.game-play'),
			    		'slug'	=> 'manager-game-play',
			    		'url'	=> 'game-play',
			    		'icon'	=> ' icon-loop',
			    		'route' => 'auth-get-game-play',
			    		'sub'	=> ''
			    	],
			    	[
	    				'id'	=> '',
	    				'use' 	=> true,
			    		'name' 	=> Lang::get('menu.history-iap'),
			    		'slug'	=> 'manager-history-iap',
			    		'url'	=> 'history-iap',
			    		'icon'	=> ' icon-loop',
			    		'route' => 'auth-get-history-iap',
			    		'sub'	=> ''
			    	],
			    	[
	    				'id'	=> '',
	    				'use' 	=> true,
			    		'name' 	=> Lang::get('menu.config-system'),
			    		'slug'	=> 'manager-config-system',
			    		'url'	=> 'config-system',
			    		'icon'	=> 'icon-settings',
			    		'route' => 'auth-get-config-system',
			    		'sub'	=> ''
			    	]
		    	],
			'top-menu' => [
			]

	    );


		$count = COUNT($data['hor-menu']);
        for ($i=0; $i < $count; $i++) {
        	$slug = $data['hor-menu'][$i]['slug'];

        	if(isset(Cache::get('module')[$slug]) && isset(Cache::get('permissionRoles')[Auth::user()['roles']]) ){

	            if(Cache::get('permissionRoles')[Auth::user()['roles']][Cache::get('module')[$slug]]['read'] == 1){

	                $data['hor-menu'][$i]['use'] = true;
	                if($data['hor-menu'][$i]['sub'] != ''){
	                	$count_sub = COUNT($data['hor-menu'][$i]['sub']);

	                	for ($j=0; $j < $count_sub; $j++) {
	                		$sub_slug = $data['hor-menu'][$i]['sub'][$j]['slug'];

	                		if(isset(Cache::get('module')[$sub_slug])){
	                			if(Cache::get('permissionRoles')[Auth::user()['roles']][Cache::get('module')[$sub_slug]]['read'] == 1){
	                				$data['hor-menu'][$i]['sub'][$j]['use'] = true;
	                			}else {
	                				$data['hor-menu'][$i]['sub'][$j]['use'] = false;
	                			}
	                		}
	                	}
	                }
	            }else {
	                $data['hor-menu'][$i]['use'] = false;
	            }
            }
        }

		$actual_link = $_SERVER['REQUEST_URI'];
		$list_segment = explode('/', $actual_link);
		// $ModelsUser = new ModelsUser();
		// $dataUser = $ModelsUser->getDataById(Auth::user()->id);

        $view->with('data', array(
        	'data' => $data,
        	'actual_link' => $actual_link,
        	'list_segment' => $list_segment,
        	// 'user'	=> $dataUser['response'][0],
        ));
    }
};
?>