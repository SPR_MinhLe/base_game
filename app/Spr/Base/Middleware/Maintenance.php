<?php

namespace Spr\Base\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use DB;
use Spr\Base\Models\PermissionRoles as ModelPermissionRoles;
use Spr\Base\Models\Module as ModelModule;
use Spr\Base\Models\Roles as ModelRoles;
use Spr\Base\Controllers\ExchangeRates\ExchangeRates;

use App\Http\Models\ProductCategories as ModelProductCategories;
// use Spr\Base\Models\Media as ModelMedia;
use Spr\Base\Response\Response as ResponseBase;
use App\Http\Controllers\GroupMediaController;
use Session;
use Cache;
use Config;
use Request;
use Auth;
class Maintenance
{
     /**
     * The Guard implementation.
     *
     * @var Guard
     */
     // protected $auth;

     /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
     public function __construct()
     {
          // $this->auth = $auth;
     }

     /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
          Cache::flush();
          // Cache::flush(); remove all cache
          $this->setCacheModule();
          $this->setCacheRoles();
          $this->setCachePermissionRoles();

          $maintenance_system = Config::get('spr.system.maintenance.system');

          if($maintenance_system){

               if($request->is('api/*')){

               return response()->json(array(

                    'meta' => array(
                         'code' => '500',
                         'msg'  => Lang::get('message.error.00015'),
                         'success' => false
                    ),
                    'response' => null
               ));

               }else {

                return redirect()->route('maintenance');
               }

          }else {

               return $next($request);
          }
     }

     public function setCategoriesProduct() {

          if(!Cache::has('product_categories')){

               $ModelProductCategories = new ModelProductCategories();
               $ModelProductCategories->getProductCategoriesForCache();
          }
     }

     public function setSessionEchaneRate() {

          $ExchangeRatesCurrency = ExchangeRates::getExchangeRate();
          Session::put('ExchangeRateCurrency-'.Request::ip(), $ExchangeRatesCurrency['response']);
     }

     public function  setCachePermissionRoles () {

          if(Auth::check() ){

               if(!Cache::has('permissionRoles')){

                    $ModelPermissionRoles= new ModelPermissionRoles();
                    $dataPermissionRoles = $ModelPermissionRoles->getAllDataRoles();
                    $lengthData = COUNT($dataPermissionRoles['response']);
                    $arrayData = array();
                    $dataRoles = Cache::get('roles');
                    $lengthDataRoles = COUNT($dataRoles);

                    for ($i=0; $i < $lengthData; $i++) {

                         $role_id = $dataPermissionRoles['response'][$i]['roles_id'];
                         $module_id = $dataPermissionRoles['response'][$i]['module_id'];
                         $dataPermission = $this->getDataPermissionRoles($dataPermissionRoles['response'][$i]);

                         if(isset($arrayData[$role_id])){

                              $arrayData[$role_id][$module_id] = $dataPermission;
                         }else {


                              $arrayData[$role_id] = [$module_id => $dataPermission];
                         }
                    }
                    Cache::forget('permissionRoles');
                    Cache::forever('permissionRoles', $arrayData);
               }
          }
     }

     public function getDataPermissionRoles($data) {

          $dataReturn = [];

          foreach ($data as $key => $value) {
               if($key != 'id' && $key != 'module_id' && $key != 'roles_id') {
                    $dataReturn[$key] = $value;
               }
          }
          return $dataReturn;
     }

     public function  setCacheModule () {

          if(!Cache::has('module')){

               $ModelModule = new ModelModule();
               $dataModelModule = $ModelModule->getAllData();
               if(!empty($dataModelModule['response'])) {

                    $lengthData = COUNT($dataModelModule['response']);
                    $arrayData = array();

                    for ($i=0; $i < $lengthData; $i++) {

                         $arrayData[$dataModelModule['response'][$i]['name']] = $dataModelModule['response'][$i]['id'];
                    }
                    Cache::forever('module', $arrayData);
               }
          }
     }

     public function  setCacheRoles () {

          if(!Cache::has('roles')){

               $ModelRoles = new ModelRoles();
               $dataModelRoles = $ModelRoles->getAllData();

               if(!empty($dataModelRoles['response'])) {

                    $lengthData = COUNT($dataModelRoles['response']);
                    $arrayData = array();
                    for ($i=0; $i < $lengthData; $i++) {

                         $arrayData[$dataModelRoles['response'][$i]['id']] = array(
                         'name' => $dataModelRoles['response'][$i]['name'],
                         'slug' => $dataModelRoles['response'][$i]['slug']
                         );
                    }
                    Cache::forever('roles', $arrayData);
               }
          }
     }
}
