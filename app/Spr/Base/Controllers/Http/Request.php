<?php
namespace Spr\Base\Controllers\Http;

use Spr\Base\Validates\ValidateInput;
use Input;
use Lang;
use Spr\Base\Controllers\Helper as HelperController;

class Request  {

	public function getDataRequest ($config) {

		$dataOutputGetDataRequest = array();
		try{

			$lengthKey = COUNT( $config['keys'] );

			for($i = $lengthKey - 1; $i >= 0; $i--){

				$value = $this->getValueOfParam($config['keys'][$i]);

				$config['keys'][$i]['validate'] = $this->getValidateOfParam($value, $config['keys'][$i]);

				if($config['keys'][$i]['key'] == 'file'){
					$file = Input::file( 'file' );

			        if ( !empty( $file ) )
			        {
			            foreach ( $file as $key => $image ) // add individual rules to each image
			            {
			                $config['keys'][$i]['validate']['rules'][ sprintf( 'file.%d', $key ) ] = 'required|mimes:jpeg,bmp,png';
			            }
			        }
					array_push($dataOutputGetDataRequest, array(
												'key'	=> $config['keys'][$i]['key'],
												'value' => $value,
												'validate' =>  $config['keys'][$i]['validate']
												));
				}else {
					array_push($dataOutputGetDataRequest, array(
													'key'	=> $config['keys'][$i]['key'],
													'value' => $value,
													'validate' =>  $config['keys'][$i]['validate']
													));
				}

			}
		} catch (Exception $e) {

			$Response = new Response();
			return $Response->response('500', Lang::get('message.error.00013'), '', false);
		}
		return $dataOutputGetDataRequest;
	}

	// public function getFilesRequest () {
	// 	$rules = [ 'files' => 'required|array|max:4000' ];

 //        $images = Input::file( 'files' );

 //        if ( !empty( $images ) )
 //        {
 //            foreach ( $images as $key => $image ) // add individual rules to each image
 //            {
 //                $rules[ sprintf( 'images.%d', $key ) ] = 'required|image';
 //            }
 //        }
 //        return $rules;
	// }

	public function getValueOfParam ($configParam) {

		$value = null;
		if( Input::file($configParam['key']) != null ){
			$value = Input::file($configParam['key']);
		}
		else{
			$value = Input::get($configParam['key']);
		}

		if(isset($configParam['validate']['htmlentities']) && $configParam['validate']['htmlentities']) $value = HelperController::html_entity_encode_XSS_attack($value);

		if($value == null || $value == '') {

			$value = $configParam['default'];
		}
		return $value;
	}

	public function getValidateOfParam ($valueOfParam, $configParam){

		$newArray = $configParam['validate'];
		if( !empty( $newArray ) && isset($newArray['messages'])) {

			$newArray['param'][$configParam['key']] = $valueOfParam;

			foreach ($newArray['messages'] as $key => $message) {

				$newArray['messages'][$key] = Lang::get($message);
			}
		}
		return $newArray;
	}

	public function getXml ($source) {
		$xmlData = NULL;

		try {
			$xmlRequest = @file_get_contents($source);
			if($xmlRequest === FALSE){
				$xmlData = false;
			}else {
				$xmlData = NULL;
				$p = xml_parser_create();
				xml_parse_into_struct($p,$xmlRequest , $xmlData);
			}
		} catch (Exception $e) {
			$xmlData = false;
		}


		return $xmlData;
	}

	public function getJsonFromUrl ($source) {

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $source);
		$result = curl_exec($ch);
		curl_close($ch);

		$obj = json_decode($result);
		return $obj;
	}


	public static function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {

	    $output = NULL;
	    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
	        $ip = $_SERVER["REMOTE_ADDR"];
	        if ($deep_detect) {
	            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
	                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
	                $ip = $_SERVER['HTTP_CLIENT_IP'];
	        }
	    }
	    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
	    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
	    $continents = array(
	        "AF" => "Africa",
	        "AN" => "Antarctica",
	        "AS" => "Asia",
	        "EU" => "Europe",
	        "OC" => "Australia (Oceania)",
	        "NA" => "North America",
	        "SA" => "South America"
	    );

	    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {

	        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

	        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {

	            switch ($purpose) {
	                case "location":
	                    $output = array(
	                        "city"           => @$ipdat->geoplugin_city,
	                        "state"          => @$ipdat->geoplugin_regionName,
	                        "country"        => @$ipdat->geoplugin_countryName,
	                        "country_code"   => @$ipdat->geoplugin_countryCode,
	                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
	                        "continent_code" => @$ipdat->geoplugin_continentCode
	                    );
	                    break;
	                case "address":
	                    $address = array($ipdat->geoplugin_countryName);
	                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
	                        $address[] = $ipdat->geoplugin_regionName;
	                    if (@strlen($ipdat->geoplugin_city) >= 1)
	                        $address[] = $ipdat->geoplugin_city;
	                    $output = implode(", ", array_reverse($address));
	                    break;
	                case "city":
	                    $output = @$ipdat->geoplugin_city;
	                    break;
	                case "state":
	                    $output = @$ipdat->geoplugin_regionName;
	                    break;
	                case "region":
	                    $output = @$ipdat->geoplugin_regionName;
	                    break;
	                case "country":
	                    $output = @$ipdat->geoplugin_countryName;
	                    break;
	                case "countrycode":
	                    $output = @$ipdat->geoplugin_countryCode;
	                    break;
	            }
	        }
	    }
	    return $output;
	}
}
?>