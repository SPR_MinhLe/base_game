@extends('layouts/admin/master')

@section('title')
    <title>{{ Lang::get('manager/config-system.title.title') }}</title>
@endsection

@section('css')

    <link href="{{ URL::asset('assets/global/plugins/datatables/datatables.min.css" rel="stylesheet') }}" type="text/css" />
    <link href="{{ URL::asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css" >

        th.sorting*>a {
            display: block;
            width: 100%;
        }
        th.un-sort>a, th.sorting>a, th.sorting_asc>a, th.sorting_desc>a {
            text-decoration: none;
            color: black;
        }
        th.un-sort>a:hover, th.un-sort>a:focus,th.sorting>a:hover, th.sorting>a:focus, th.sorting_asc>a:hover, th.sorting_asc>a:focus, th.sorting_desc>a:hover, th.sorting_desc>a:focus{
            color: black;
        }
        th>a>p {
            margin: 0px !important;
        }
        #table-permission-role tr td, #table-permission-role tr th{
            max-width: 90px;
            min-width: 90px;
            word-wrap: break-word;
            text-align: center !important;
        }

        @media screen and (max-width: 900px) {
            #table-permission-role tr td:first; {
                text-align: left !important;
            }
        }

        .error {
            color: red;
        }
    </style>
@endsection

@section('js')
    <script src="{{ URL::asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/global/plugins/datatables/datatables.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>

    <!-- Datatable js -->
    <script src="{{ URL::asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <!--End Datatable js -->
    <script src="{{ URL::asset('js/lib/validate.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/manage/items/items.js') }}" ></script>
    <script type="text/javascript">
        Items.init();
    </script>

@endsection

@section('content')
   <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">{{ Lang::get('manager/config-system.title.title') }}</span>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-line boxless margin-bottom-20">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1" data-toggle="tab"> {{ Lang::get('manager/config-system.title.coint') }} </a>
                            </li>
                            <li>
                                <a href="#tab_2" data-toggle="tab"> {{ Lang::get('manager/config-system.title.list-server') }} </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <form action="{{ URL::Route( 'auth-post-update-config-coint-system' ) }}" method="POST" class="horizontal-form">
                                <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" id ="_id" name = "_id" value ="{{ (string)Cache::get( 'api-config' )['_id']}}">
                                    <div class="form-body">
                                        <h3 class="form-section">{{ Lang::get('manager/config-system.title.coint-bet') }}</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{ Lang::get('manager/config-system.title.coint-bet-win') }}</label>
                                                    <input type="text" class="form-control" name="coint_bet_win" value="{{ Cache::get( 'api-config' )['coint']['bet']['win'] }}">
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{ Lang::get('manager/config-system.title.coint-bet-lose') }}</label>
                                                    <input type="text" name="coint_bet_lose" class="form-control"  value="{{ Cache::get( 'api-config' )['coint']['bet']['lose'] }}">
                                                    <span class="help-block">  </span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <h3 class="form-section">{{ Lang::get('manager/config-system.title.coint-reward') }}</h3>

                                        <div class="row">
                                            <h4 class="form-section">{{ Lang::get('manager/config-system.title.coint-reward-singleMode') }}</h4>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{ Lang::get('manager/config-system.title.coint-reward-singleMode-video') }}</label>
                                                    <input type="text" class="form-control" name="coint_reward_singleMode_video" value="{{ Cache::get( 'api-config' )['coint']['reward']['singleMode']['video'] }}"> 
                                                    <span class="help-block">  </span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{ Lang::get('manager/config-system.title.coint-reward-singleMode-facebook') }}</label>
                                                    <input type="text" class="form-control" name="coint_reward_singleMode_facebook" value="{{ Cache::get( 'api-config' )['coint']['reward']['singleMode']['facebook'] }}"> 
                                                    <span class="help-block">  </span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <h4 class="form-section">{{ Lang::get('manager/config-system.title.coint-reward-muntiplayer') }}</h4>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{ Lang::get('manager/config-system.title.coint-reward-muntiplayer-video') }}</label>
                                                    <input type="text" class="form-control" name="coint_reward_muntiplayer_video"  value="{{ Cache::get( 'api-config' )['coint']['reward']['Muntiplayer']['video'] }}"> 
                                                    <span class="help-block">  </span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{ Lang::get('manager/config-system.title.coint-reward-muntiplayer-facebook') }}</label>
                                                    <input type="text" class="form-control" name="coint_reward_muntiplayer_facebook"  value="{{ Cache::get( 'api-config' )['coint']['reward']['Muntiplayer']['facebook'] }}"> 
                                                    <span class="help-block">  </span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <h3 class="form-section">{{ Lang::get('manager/config-system.title.coint-invited') }}</h3>
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label>{{ Lang::get('manager/config-system.title.coint-invited-facebook') }}</label>
                                                    <input type="text" class="form-control" name="coint_invited_facebook" value="{{ Cache::get( 'api-config' )['coint']['invited']['facebook'] }}">
                                                    <span class="help-block">  </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button type="button" class="btn default">Cancel</button>
                                        <button type="submit" class="btn blue">
                                            <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tab_2">
                                <form action="{{ URL::Route( 'auth-post-update-config-server-list' ) }}" method="POST" class="horizontal-form">
                                <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" id ="_id" name = "_id" value ="{{ (string)Cache::get( 'api-config' )['_id'] }}">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{ Lang::get('manager/config-system.title.server-api') }}</label>
                                                    <input type="text" class="form-control" name="server_api" value="{{ Cache::get( 'api-config' )['server_list']['api'] }}" >
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{ Lang::get('manager/config-system.title.server-real_time') }}</label>
                                                    <input type="text" name="server_real_time" class="form-control"  value="{{ Cache::get( 'api-config' )['server_list']['real_time'] }}" >
                                                    <span class="help-block">  </span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button type="button" class="btn default">Cancel</button>
                                        <button type="submit" class="btn blue">
                                            <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection