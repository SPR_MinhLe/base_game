<?php

	return [

		'web'	=> [

			'error' => [
				'000001' => 'Fields is required',
				'000002' => 'Length of data is too short',
				'000003' => 'Length of data is too long',
				'000004' => 'This field must be number',
				'000005' => 'Login failed. The username or password is incorrect.',
				'000006' => "Can't find user",

			],
			'success' => [
				'000001' => 'Block user success',
			]
		],
		'api'	=> [

			'error' => [
				'000001' => "Fields is required",
				'000002' => "Token Mismatch",
				'000003' => "is not valid",
				'000004' => "not found",
				'000005' => "The game is over",
				'000006' => "The game already exists",
				'000007' => "does not exist",
				'000008' => "can't use item",
				'000009' => "not enough coint",
				'000010' => "You already do this.",
				'000011' => "You can't invite friends."


			],
			'success' => [

			]

		]
	];

?>
