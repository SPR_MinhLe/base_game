<?php

	return [

		'title'	=> [
			'title' => 'Manager levels',
			'level' => 'Level',
			'status' => 'Status',
			'unlock_requirement' => 'Requirement unlock',
			'fall_speed' => 'Fall speed',
			'brick_total' => 'Total brick',
			'rarity_J' => 'Brick ratio J',
			'rarity_L' => 'Brick ratio L',
			'rarity_l' => 'Brick ratio l',
			'rarity_Z' => 'Brick ratio Z',
			'rarity_S' => 'Brick ratio S',
			'rarity_O' => 'Brick ratio O',
			'rarity_T' => 'Brick ratio T',
			'action' => 'Action',
		],
	];


?>
