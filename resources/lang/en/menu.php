<?php

return [

	'dashboard'				=> 'Dashboard',
	'register-user'			=> 'Register user',
	'roles'					=> 'Manager roles',
	'items'					=> 'Manager items',
	'levels'				=> 'Manager levels',
	'game-play'				=> 'Manager game play',
	'config-system'			=> 'Config system',
	'users'					=> "Manager users",
	'history-iap'			=> "History IAP",

];