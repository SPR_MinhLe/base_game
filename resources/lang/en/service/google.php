<?php
	
	return [

		'error' => [
			'0001' => "Can't verify your order",
			'0002' => "Invalid product",
			'0003' => "Invalid transaction",
		]
	];

?>