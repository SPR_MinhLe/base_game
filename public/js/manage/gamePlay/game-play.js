/**
Custom module for you to write your own javascript functions
**/
var GamePlay = function () {

	var elem_block_loadding;
	var elm_active;

    // public functions
    return {

        //main function

        init: function () {

            $(document).ready(function(){

                var form  = $('.form-edit');
                var rules = {

                    _id: {
                        required: true
                    },
                    description: {
                        required: true,
                        maxlength : 2000
                    },
                    amount: {
                        required: true,
                        number: true,
                    },
                    bonus: {
                        required: true,
                        number: true,
                    }
                }
                Validate.base_validate(form, rules);
            });
        }
    };

}();




/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();