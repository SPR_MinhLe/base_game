/**
Custom module for you to write your own javascript functions
**/
var Level = function () {

	var elem_block_loadding;
	var elm_active;

    // public functions
    return {

        //main function

        init: function () {

            $(document).ready(function(){

                var form  = $('.form-action');
                var rules = {

                    _id: {
                        required: true
                    },
                    unlock_requirement: {
                        required: true,
                        number: true,
                        max:9
                    },
                    fall_speed: {
                        required: true,
                        number: true,
                    },
                    brick_total: {
                        required: true,
                        number: true,
                    },
                    rarity_J: {
                        required: true,
                        number: true,
                        max:100
                    },
                    rarity_L: {
                        required: true,
                        number: true,
                        max:100
                    },
                    rarity_l: {
                        required: true,
                        number: true,
                        max:100
                    },
                    rarity_Z: {
                        required: true,
                        number: true,
                        max:100
                    },
                    rarity_S: {
                        required: true,
                        number: true,
                        max:100
                    },
                    rarity_O: {
                        required: true,
                        number: true,
                        max:100
                    },
                    rarity_T: {
                        required: true,
                        number: true,
                        max:100
                    }
                }
                Validate.base_validate(form, rules);
            });
        }
    };

}();




/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();