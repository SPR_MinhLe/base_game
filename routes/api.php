<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'middleware' => 'config.api'], function () {

	Route::group(['as' => 'guest-'], function () {

		Route::group(['as' => 'get-'], function () {

		});

		Route::group(['as' => 'post-'], function () {

			Route::post('login-with-email', ['as' => 'login-with-email', function() {

				$config = Config::get('api.actions.param.register');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
				$results					= App::make('App\Http\Controllers\UserController')->getDataByEmail($data_output_validate_param);

				return response()->json($results);

			}]);

			Route::post('login-facebook', ['as' => 'login-facebook', function() {

				$config = Config::get('api.actions.param.login_facebook');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateAuthApi')->validate($data_output_get_param);
				$results					= App::make('App\Http\Controllers\UserController')->linkFacebook($data_output_validate_param);

				return response()->json($results);

			}]);
		});
	});

	Route::group(['middleware' => 'auth.api','as' => 'auth-'], function () {

		Route::group(['as' => 'get-'], function () {


		});

		Route::group(['as' => 'post-'], function () {

			Route::post('rank', ['as' => 'get-rank', function() {

				$config = Config::get('api.actions.param.get-rank');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateAuthApi')->validate($data_output_get_param);
				$results					= App::make('App\Http\Controllers\RankController')->getListRank($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('hight-coint', ['as' => 'get-hight-coint', function() {

				$config = Config::get('api.actions.param.get-hight-coint');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateAuthApi')->validate($data_output_get_param);
				$results					= App::make('App\Http\Controllers\HightCointController')->getHightCoint($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('start-game', ['as' => 'start-game', function() {

				$config = Config::get('api.actions.param.start-game');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateActionGame')->validateStartGame($data_output_get_param);
				$results					= App::make('App\Http\Controllers\ActionGameController')->startGame($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('end-game', ['as' => 'end-game', function() {

				$config = Config::get('api.actions.param.end-game');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateActionGame')->validateEndGame($data_output_get_param);
				$results					= App::make('App\Http\Controllers\ActionGameController')->endGame($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('end-game-multi-player', ['as' => 'end-game-multi-player', function() {

				$config = Config::get('api.actions.param.end-game');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateActionGame')->validateEndGame($data_output_get_param);
				$results					= App::make('App\Http\Controllers\ActionGameController')->endGameMultiPlayer($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('get-coint', ['as' => 'get-coint', function() {

				$config = Config::get('api.actions.param.get-coint');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateActionGame')->validateGetCoint($data_output_get_param);
				$results					= App::make('App\Http\Controllers\ActionGameController')->getCoint($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('use-item', ['as' => 'use-item', function() {

				$config = Config::get('api.actions.param.use-item');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateActionGame')->validateUseItem($data_output_get_param);
				$results					= App::make('App\Http\Controllers\ActionGameController')->useItem($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('user-information', ['as' => 'user-information', function() {

				$config = Config::get('api.actions.param.user-information');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateAuthApi')->validate($data_output_get_param);
				$results					= App::make('App\Http\Controllers\UserController')->getUserInformation($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('history', ['as' => 'history', function() {

				$config = Config::get('api.actions.param.history');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateAuthApi')->validate($data_output_get_param);
				$results					= App::make('App\Http\Controllers\HistoryMultiPaylerController')->getListHistoryByUserId($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('reward-more-coin', ['as' => 'reward-more-coin', function() {

				$config = Config::get('api.actions.param.reward-more-coin');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateActionGame')->validateRewardMoreCoin($data_output_get_param);
				$results					= App::make('App\Http\Controllers\ActionGameController')->rewardMoreCoin($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('invited-facebook', ['as' => 'invited-facebook', function() {

				$config = Config::get('api.actions.param.invited-facebook');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateInvitedFriendsFacebook')->invitedFriends($data_output_get_param);
				$results					= App::make('App\Http\Controllers\FacebookController')->invitedFriends($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('iap', ['as' => 'iap', function() {

				$config = Config::get('api.actions.param.iap_google');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateIAP')->validateTransaction($data_output_get_param);
				$results					= App::make('App\Http\Controllers\IapController')->google_iap($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('iap-app-store', ['as' => 'iap-app-store', function() {

				$config = Config::get('api.actions.param.iap_app_store');
				$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
				$data_output_validate_param	= App::make('App\Http\Validates\ValidateIAP')->validateTransactionAppStore($data_output_get_param);
				$results					= App::make('App\Http\Controllers\IapController')->app_store_iap($data_output_validate_param);

				return response()->json($results);
			}]);

			Route::post('get-config', ['as' => 'get-config', function() {

				$data_config = [
					'meta' => [
						'success' => true,
						'code' => 200,
						'mesage' => [],
					],
					'response' => [
						'activity'	=> Cache::get('api-activity'),
						'game_play'	=> Cache::get('api-game-play'),
						'item'		=> Cache::get('item'),
						'config'	=> Cache::get('api-config'),
						'level'		=> Cache::get('api-level'),
						'product'	=> Cache::get('product')
					]
				];

				return response()->json($data_config);
			}]);
		});
	});
});

