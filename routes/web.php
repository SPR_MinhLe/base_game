<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use Spr\Base\Response\Response;
use Spr\Base\Controllers\Helper as HelperController;

Route::get('maintenance',['as' => 'maintenance', function(){

	if(Config::get('spr.system.maintenance.system')){

		return view('maintenance.maintenance');

	}else {

		return redirect('/');
	}
}]);


Route::group(['middleware' => 'maintenance'], function () {

	Route::group(['prefix' => ''], function () {

		Route::group(['middleware' => ['auth:manager','config.api'], 'as' => 'auth-'], function () {

			Route::group(['as' => 'get-'], function () {

				Route::get('/',  ['as' => 'dashboard', function() {

						return view('pages.manager.index');
				}]);

				Route::get('/change-password',  ['as' => 'change-password', function() {

						return view('pages.manager.change_password');
				}]);

				Route::get('/logout',  ['as' => 'logout', function() {

					Auth::guard('manager')->logout();
					return redirect()->route('guest-get-login');
				}]);

				Route::group(['as' => 'permission-'], function () {

					Route::get('roles/' , ['as' => 'roles', function () {

						// $permission = HelperController::checkPermission(Auth::guard('web')->user()->roles, 'manager-roles');
						// if($permission){

							$config = Config::get('spr.param.manager.permission.roles');
							$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
							$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
							$results					= App::make('Spr\Base\Controllers\Permission\Roles')->getData($data_output_validate_param);

							return view('pages.manager.roles')->with('data', $results);
						// }else {
						// 	return view('errors.550');
						// }
					}]);
				});

				Route::get('/levels',  ['as' => 'levels', function() {


						$config = Config::get('spr.param.manager.level');
						$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
						$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
						$results					= App::make('App\Http\Controllers\LevelController')->getDataManager($data_output_validate_param);
						return view('pages.manager.level')->with('data' , $results['response']);
				}]);

				Route::get('/game-play',  ['as' => 'game-play', function() {


					$config = Config::get('spr.param.manager.level');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
					$results					= App::make('App\Http\Controllers\GamePlayController')->getDataManager($data_output_validate_param);
					return view('pages.manager.game_play')->with('data' , $results['response']);
				}]);

				Route::get('/users',  ['as' => 'users', function() {

					$config = Config::get('spr.param.manager.level');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
					$results					= App::make('App\Http\Controllers\UserController')->getDataManager($data_output_validate_param);
					return view('pages.manager.users')->with('data' , $results['response']);
				}]);

				Route::get('/items',  ['as' => 'items', function() {


					$config = Config::get('spr.param.manager.level');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
					$results					= App::make('App\Http\Controllers\ItemsController')->getDataManager($data_output_validate_param);
					return view('pages.manager.items')->with('data' , $results['response']);
				}]);

				Route::get('/config-system',  ['as' => 'config-system', function() {


						return view('pages.manager.config-system');
				}]);

				Route::get('/history-iap',  ['as' => 'history-iap', function() {

					$config = Config::get('spr.param.manager.iap');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
					$results					= App::make('App\Http\Controllers\IapController')->getDataHistory($data_output_validate_param);
					return view('pages.manager.history-iap')->with('data' , $results['response']);
				}]);

			});

			Route::group(['as' => 'post-'], function () {

				Route::post('/change-admin-password' , ['as' => 'change-admin-password', function () {

			  		$config 					= Config::get('spr.param.manager.admin.change_admin_password');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
			        $results    			 	= App::make('App\Http\Controllers\Auth\ManagerController')->changePassword($data_output_validate_param);

			       	if ($results['meta']['success']) {

						return Redirect::route('auth-get-change-password')->with('message', $results);

					}
					// else {

					// 	return Redirect::route('web-get-404');
					// }
			  	}]);
			  	
				Route::post('update-roles', ['as' => 'update-roles', function() {

					// $permission = HelperController::checkPermission(Auth::guard('web')->user()->roles, 'manager-roles', 'write');
					// if($permission){

						$config = Config::get('spr.param.manager.permission.newRole');
						$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
						$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
						$results					= App::make('App\Http\Controllers\RoleController')->updateRole($data_output_validate_param);

						return redirect()->back();
					// }else {

					// 	return view('errors.550');
					// }
				}]);

				Route::post('update-permission', ['as' => 'update-permission', function() {

					$config 					= Config::get('spr.param.manager.permission.ajaxGetPermission');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
					$results 					= App::make('App\Http\Controllers\PermissionController')->updatePermission($data_output_validate_param);

					return json_encode($results);
				}]);

				Route::post('get-permission', ['as' => 'get-permission', function() {

					$config 					= Config::get('spr.param.manager.permission.getPermissionRoles');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
					$results 					= App::make('App\Http\Controllers\PermissionRolesController')->getData($data_output_validate_param);

					return json_encode($results);
				}]);

				Route::post('delete-roles', ['as' => 'delete-roles', function() {

					$config 					= Config::get('spr.param.manage.roles.deleteRoles');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
					$results 					= App::make('App\Http\Controllers\RoleController')->deleteData($data_output_validate_param);

					return json_encode($results);
				}]);

				Route::post('/update-level',  ['as' => 'update-level', function() {


						$config = Config::get('spr.param.manager.level-update');
						$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
						$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
						$results					= App::make('App\Http\Controllers\LevelController')->updateLevel($data_output_validate_param);
						return redirect()->route('auth-get-levels');
				}]);

				Route::post('/update-game-play',  ['as' => 'update-game-play', function() {


						$config = Config::get('spr.param.manager.update-game-play');
						$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
						$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
						$results					= App::make('App\Http\Controllers\GamePlayController')->updateGamePlay($data_output_validate_param);
						return redirect()->route('auth-get-game-play');
				}]);

				Route::post('/update-item',  ['as' => 'update-item', function() {

						$config = Config::get('spr.param.manager.update-item');
						$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
						$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
						$results					= App::make('App\Http\Controllers\ItemsController')->updateItems($data_output_validate_param);
						return redirect()->route('auth-get-items');
				}]);

				Route::post('/update-config-coint-system',  ['as' => 'update-config-coint-system', function() {

						$config = Config::get('spr.param.manager.update-config-coint-system');
						$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
						$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
						$results					= App::make('App\Http\Controllers\ConfigSystemController')->updateConfigCoint($data_output_validate_param);
						return redirect()->route('auth-get-config-system');
				}]);

				Route::post('/update-config-server-list',  ['as' => 'update-config-server-list', function() {

						$config = Config::get('spr.param.manager.update-config-server-list');
						$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
						$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
						$results					= App::make('App\Http\Controllers\ConfigSystemController')->updateServerList($data_output_validate_param);
						return redirect()->route('auth-get-config-system');
				}]);

				Route::post('/block-player',  ['as' => 'block-player', function() {

						$config = Config::get('spr.param.manager.block-user');
						$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
						$data_output_validate_param	= App::make('App\Http\Validates\Users')->validateBlock($data_output_get_param);
						$results					= App::make('App\Http\Controllers\UserController')->blockUser($data_output_validate_param);
						return redirect()->route('auth-get-users');
				}]);
			});
		});



		Route::group(['middleware' => 'guest', 'as' => 'guest-'], function () {

			Route::group(['as' => 'get-'], function () {

				Route::get('login' , ['as' => 'login', function () {

					return view('pages.manager.login');
				}]);

				Route::get('reset-password' , ['as' => 'reset-password', function () {

					$config 					= Config::get('spr.param.web.password.check_reset_password');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
					$results 					= App::make('App\Http\Controllers\UserController')->checkResetPassword($data_output_validate_param);

					if ($results == true) {

						return view('login.reset_pass');
					} else {

						return redirect()->route('web-get-404');
					}
				}]);

			});

			Route::group(['as' => 'post-'], function () {

				Route::post('login' , ['as' => 'login', function () {

					$config 					= Config::get('spr.param.manager.login.login');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
			        $results    			 	= App::make('App\Http\Controllers\Auth\ManagerController')->login($data_output_validate_param);

					if ( $results['meta']['success'] ) {
						if(Auth::guard('manager')->user()->blocked){

							Auth::guard('manager')->logout();
							return Redirect::back()->withErrors(['email'=>'tài khoản của bạn đã bị khóa!']);
						}

						return Redirect::route('auth-get-dashboard');
					}else{

						return Redirect::back()->withErrors($results['meta']['msg']);
					}
				}]);

				Route::post('/reset-password' , ['as' => 'reset-password', function () {
			  		$config 					= Config::get('spr.param.web.password.reset_password');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
			        $results    			 	= App::make('App\Http\Controllers\UserController')->resetPassword($data_output_validate_param);

			        if($results['meta']['success']) {

			       		return redirect()->route('web-get-thanks');
			        }else {

			        	return Redirect::route('guest-get-login')->withErrors($results['meta']['msg']);
			        }

			  	}]);


			  	Route::post('/change-password' , ['as' => 'change-password', function () {

			  		$config 					= Config::get('spr.param.web.password.action_reset_password');
					$data_output_get_param 		= App::make('Spr\Base\Controllers\Http\Request')->getDataRequest($config);
					$data_output_validate_param	= App::make('Spr\Base\Validates\Helper')->baseValidate($data_output_get_param);
			        $results    			 	= App::make('App\Http\Controllers\UserController')->actionResetPassword($data_output_validate_param);

			       	return redirect()->route('guest-get-login');
			  	}]);
			});
		});
	});
});